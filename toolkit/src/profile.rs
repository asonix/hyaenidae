use crate::{banner::Banner, icon::Icon, image::Image};

#[derive(Clone, Debug)]
pub struct Profile {
    pub(crate) handle: String,
    pub(crate) display_name: Option<String>,
    pub(crate) description: Option<String>,
    pub(crate) icon: Icon,
    pub(crate) banner: Banner,
    dark: bool,
}

impl Profile {
    pub fn new(handle: String, href: String) -> Self {
        Profile {
            handle,
            display_name: None,
            description: None,
            icon: Icon::new(href),
            banner: Banner::new(),
            dark: false,
        }
    }

    pub fn display_name(mut self, display_name: &str) -> Self {
        self.display_name = Some(display_name.to_owned());
        self
    }

    pub fn description(mut self, description: &str) -> Self {
        self.description = Some(description.to_owned());
        self
    }

    pub fn icon_image(self, image: impl Image + 'static) -> Self {
        Profile {
            icon: self.icon.image(image),
            ..self
        }
    }

    pub fn banner_image(self, image: impl Image + 'static) -> Self {
        Profile {
            banner: self.banner.image(image),
            ..self
        }
    }

    pub fn dark(self, dark: bool) -> Self {
        Profile {
            icon: self.icon.dark(dark),
            banner: self.banner.dark(dark),
            dark,
            ..self
        }
    }

    pub(crate) fn class_string(&self) -> String {
        let mut classes = vec!["toolkit-profile".to_owned()];

        if self.dark {
            classes.push("toolkit-dark".to_owned());
        }

        classes.join(" ")
    }
}
