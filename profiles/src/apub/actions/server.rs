use crate::{
    apub::actions::{CreateServer, DeleteServer, UpdateServer},
    Action, Context, Error, Outbound, Required,
};

impl Action for CreateServer {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let mut changes = ctx.store.servers.create(ctx, self.domain.clone());

        changes.published(self.published);
        if let Some(updated) = self.updated {
            changes.updated(updated);
        }
        if let Some(title) = &self.title {
            changes.title(title);
        }
        if let Some(description) = &self.description {
            changes.description(description);
        }
        let server = changes.save()??;

        if let Some(apub) = &self.apub {
            ctx.apub.server(&apub.application_apub_id, server.id())?;

            ctx.apub.store_server_public_key(
                server.id(),
                &apub.endpoints.public_key,
                &apub.public_key,
            )?;

            ctx.apub.store_server_endpoints(
                server.id(),
                &apub.application_apub_id,
                apub.endpoints.clone(),
            )?;
        } else {
            return Ok(Some(Box::new(crate::apub::results::ServerCreated {
                server_id: server.id(),
            })));
        }

        Ok(None)
    }
}

impl Action for UpdateServer {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let server = ctx
            .store
            .servers
            .by_id(self.server_id)?
            .req("server by id")?;

        let mut changes = server.changes(ctx);
        changes.updated(self.updated);
        if let Some(title) = &self.title {
            changes.title(title);
        }
        if let Some(title_source) = &self.title_source {
            changes.title_source(title_source);
        }
        if let Some(description) = &self.description {
            changes.description(description);
        }
        if let Some(description_source) = &self.description_source {
            changes.description_source(description_source);
        }
        let server = if changes.any_changes() {
            changes.save()??
        } else {
            server
        };

        if let Some(apub) = &self.apub {
            let application_apub_id = ctx
                .apub
                .apub_for_server(server.id())?
                .req("apub id for server")?;

            ctx.apub.store_server_public_key(
                server.id(),
                &apub.endpoints.public_key,
                &apub.public_key,
            )?;

            ctx.apub.store_server_endpoints(
                server.id(),
                &application_apub_id,
                apub.endpoints.clone(),
            )?;
        }

        if ctx.is_self_server(self.server_id)? {
            return Ok(Some(Box::new(crate::apub::results::ServerUpdated {
                server_id: server.id(),
            })));
        }

        Ok(None)
    }
}

impl Action for DeleteServer {
    fn perform(&self, _: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        Err(Error::Invalid)
    }
}
