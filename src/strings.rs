use crate::ActixLoader;
use chrono::{DateTime, Duration, Utc};
use i18n_embed_fl::fl;

fn years(duration: Duration) -> Option<i64> {
    let years = duration.num_days().checked_div(365)?;

    if years > 0 {
        Some(years)
    } else {
        None
    }
}

pub(crate) fn ago(loader: &ActixLoader, datetime: DateTime<Utc>) -> String {
    let duration = Utc::now() - datetime;

    if let Some(years) = years(duration) {
        fl!(loader, "post-date-years", years = years)
    } else if duration.num_weeks() > 0 {
        fl!(loader, "post-date-weeks", weeks = duration.num_weeks())
    } else if duration.num_days() > 0 {
        fl!(loader, "post-date-days", days = duration.num_days())
    } else if duration.num_hours() > 0 {
        fl!(loader, "post-date-hours", hours = duration.num_hours())
    } else if duration.num_minutes() > 0 {
        fl!(
            loader,
            "post-date-minutes",
            minutes = duration.num_minutes()
        )
    } else if duration.num_seconds() > 0 {
        fl!(
            loader,
            "post-date-seconds",
            seconds = duration.num_seconds()
        )
    } else {
        fl!(loader, "post-date-now")
    }
}

pub(crate) fn time(datetime: DateTime<Utc>) -> String {
    datetime.format("%B %d, %Y, %I:%M %P").to_string()
}

pub(crate) fn ago_small(loader: &ActixLoader, datetime: DateTime<Utc>) -> String {
    let duration = Utc::now() - datetime;

    if let Some(years) = years(duration) {
        fl!(loader, "post-date-years-small", years = years)
    } else if duration.num_weeks() > 0 {
        fl!(
            loader,
            "post-date-weeks-small",
            weeks = duration.num_weeks()
        )
    } else if duration.num_days() > 0 {
        fl!(loader, "post-date-days-small", days = duration.num_days())
    } else if duration.num_hours() > 0 {
        fl!(
            loader,
            "post-date-hours-small",
            hours = duration.num_hours()
        )
    } else if duration.num_minutes() > 0 {
        fl!(
            loader,
            "post-date-minutes-small",
            minutes = duration.num_minutes()
        )
    } else if duration.num_seconds() > 0 {
        fl!(
            loader,
            "post-date-seconds-small",
            seconds = duration.num_seconds()
        )
    } else {
        fl!(loader, "post-date-now-small")
    }
}
