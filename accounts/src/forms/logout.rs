use crate::{to_home, Authenticated, State, User};
use actix_session::Session;
use actix_web::{dev::Payload, web, FromRequest, HttpRequest, HttpResponse};
use futures::future::LocalBoxFuture;

pub type LogoutPageArgs = (Authenticated, web::Data<State>);

pub type LogoutArgs = (Authenticated, web::Data<State>, Session);

#[derive(Clone)]
pub struct LogoutState {
    state: State,
    pub dark: bool,
}

pub fn logout_page((_, state): LogoutPageArgs) -> LogoutState {
    LogoutState::new(&state)
}

pub fn logout((_, state, session): LogoutArgs) -> HttpResponse {
    crate::extractors::UserData::remove(&session);

    to_home(&state)
}

impl LogoutState {
    fn new(state: &State) -> Self {
        LogoutState {
            state: state.clone(),
            dark: false,
        }
    }

    pub fn dark(&mut self, dark: bool) -> &mut Self {
        self.dark = dark;
        self
    }

    pub fn logout_path(&self) -> String {
        self.state.pages.logout_path()
    }
}

impl FromRequest for LogoutState {
    type Error = actix_web::Error;
    type Future = LocalBoxFuture<'static, Result<Self, Self::Error>>;
    type Config = ();

    fn from_request(req: &HttpRequest, _: &mut Payload) -> Self::Future {
        let user_fut = User::extract(req);
        let state_fut = web::Data::<State>::extract(req);

        Box::pin(async move {
            let _ = user_fut.await?;
            let state = state_fut.await?;

            Ok(LogoutState::new(&state))
        })
    }
}
