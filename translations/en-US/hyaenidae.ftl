site-name = Hyaenidae
site-description = A simple website

nav-text = Navigation
nav-cancel = Close

browse-logged-out = Log into {site-name} to see what's happening
browse-empty = There's nothing here, be the first to post something

settings-title = Settings
settings-subtitle = Update Settings
profile-settings = Profile Settings
account-settings = Account Settings

not-found-title = 404
not-found-subtitle = Not Found
not-found-heading = We couldn't find that

error-title = Error
error-subtitle = There was an error processing your request
error-heading = {site-name} encountered a problem
error-description = You can try returning to the previous page, or continuing home

return-home-button = Return Home

cookies-heading = Accept Cookies
cookies-subheading = Review the cookie policy
cookies-description = In order to continue, you must accept the use of cookies
cookies-accept = Accept
cookies-cancel = Cancel

register-heading = Register
register-subheading = Register for {site-name}
register-username = Username
register-password = Password
register-confirmation = Password Confirmation
register-login-link = I already have an account
register-submit = Register
register-cancel = Cancel

login-heading = Login
login-subheading = Log into {site-name}
login-username = Username
login-password = Password
login-register-ling = I do not have an account
login-submit = Login
login-cancel = Cancel

file-input-selected = Selected {$fileName}

change-profiles-title = Switch Profile
change-profiles-subtitle = Select the profile you wish to use
change-profiles-select = Select {$profileName}
change-profiles-create-heading = Create a new Profile
change-profiles-create-button = Create

create-profile-title = Create Profile
create-profile-subtitle = Create a new profile on {site-name}

create-profile-next = Next
create-profile-back = Back
create-profile-skip = Skip
create-profile-skip-all = Skip All
create-profile-preview-heading = Preview

create-handle-heading = Create a Handle
create-handle-description =
    A handle is how people on {site-name} will find your account. You can make this the same as
    your username, but it isn't required.
create-handle-input = Handle
create-handle-placeholder = Handle

create-bio-heading = Create a Bio
create-bio-description =
    This is where you can talk a bit about yourself. The {create-bio-display-name} is the name
    that will be featured on your profile page, and the {create-bio-description-input} will appear
    underneath it. Your description may contain bbcode.
create-bio-display-name = Display Name
create-bio-display-name-placeholder = Display Name
create-bio-description-input = Description
create-bio-description-placeholder = Description
create-bio-bbcode = Learn about bbcode

create-icon-heading = Add an Icon
create-icon-description =
    This icon will be displayed on your profile, and next to submissions or comments you create.
create-icon-button = Select Icon

create-banner-heading = Add a Banner
create-banner-description =
    This banner will be displayed on your profile page behind your icon.
create-banner-button = Select Banner

require-login-heading = Require Login
require-login-description =
    If you would like to hide your profile from people without accounts, you can check this box to
    restrict your profile to logged-in users.
require-login-checkbox = Require Login

create-profile-finish-heading = Finished!
create-profile-finish-description =
    Congratulations! Your profile is all set up! Now you can start to browse {site-name} as a
    full-fledged member. You can always access your profile settings again to modify anything you've
    set up here.
create-profile-success = Return Home

profile-actions-heading = Profile Actions

follow-button = Follow
cancel-follow-button = Cancel Follow Request
unfollow-button = Unfollow
block-button = Block
unblock-button = Unblock
report-button = Report

drafts-button = View Drafts
edit-profile-button = Edit Profile
switch-profile-button = Switch Profile
view-profile-button = View Profile
account-settings-button = Account Settings

profile-submissions-heading = Submissions
profile-drafts-heading = Drafts

nav-submission-button = New Submission
nav-browse-button = Browse
nav-profile-button = Profile
nav-switch-profile-button = Switch Profile
nav-notifications-button = Notifications
nav-admin-button = Admin
nav-login-button = Login
nav-register-button = Register
nav-logout-button = Logout

profile-icon = {$profileName}'s icon
profile-banner = {$profileName}'s banner
file-num = file {$num}

profile-settings-title = Profile Settings
profile-settings-subtitle = Edit {$profileName}'s profile on {site-name}

update-settings-heading = Update Profile Settings
sensitive-checkbox = Show me NSFW content
dark-checkbox = Enable dark mode
update-settings-button = Save

update-profile-heading = Update Profile
update-bio-heading = Update Bio
update-bio-description =
    Update the name that appears by your posts and on the top of your profile, and the
    description that appears on your profile page.
update-bio-button = Save
update-icon-no-icon = No icon set
update-icon-heading = New Icon
update-icon-description =
    This icon appears at the top of your profile, and next to any submissions or comments you
    create.
update-icon-button = Save
update-banner-no-banner = No banner set
update-banner-heading = New Banner
update-banner-description =
    This banner appears at the top of your profile on the profile page.
update-banner-button = Save
update-require-login-heading = Require Login
update-require-login-description =
    Choose whether your profile is visible to everyone, or only logged-in users.
update-require-login-checkbox = Require Login
update-require-login-button = Save

create-submission-title = Create Submission
create-submission-subtitle = Upload a file
create-submission-heading = Select a file to post
create-submission-input = Select Image
create-submission-button = Next

submission-visibility-select = Visibility
submission-visibility-followers = Followers Only
submission-visibility-unlisted = Unlisted
submission-visibility-public = Public

submission-visibility-heading = Audience
submission-login-required = Require login to view this submission
submission-local-only = Restrict this submission to your {site-name} server

submission-sensitive-heading = Content Rating
submission-sensitive-checkbox = Mark this submission has NSFW
submission-sensitive-button = Save

update-submission-title = Edit Submission
update-submission-subtitle = Edit information or update images
update-submission-publish-title = Publish Submission
update-submission-publish-description =
    Publish this submission to your followers
update-submission-publish-button = Publish
update-submission-info-heading = Add submission information
update-submission-title-input = Title
update-submission-title-placeholder = Set a Title
update-submission-description-input = Description
update-submission-description-placeholder = Set a Description
update-submission-info-button = Save
update-submission-remove-file-heading = Remove Files
update-submission-remove-file-button = Remove
update-submission-add-file-heading = Add file
update-submission-add-file-button = Add
update-submission-view-heading = View Submission
update-submission-view-button = View

view-submission-subtitle = {$title} hosted on {site-name}
view-submission-image-error = Failed to display image
view-submission-edit-button = Edit

account-settings-heading = Account Settings
account-settings-subheading = Update account information

username-heading = Current Username: {$currentUsername}
password-input = Password
update-username-heading = Update Username
update-username-input = New Username
update-username-button = Update Username
update-password-heading = Update Password
update-password-input = New Password
confirm-password-input = Confirm New Password
update-password-button = Update Password

danger-heading = Danger

delete-account-heading = Delete Account
delete-account-confirmation = Are you sure you want to delete your account?
delete-account-button = Delete Account
delete-account-cancel = Cancel

delete-submission-button = Delete Submission
delete-profile-title = Delete Profile
delete-profile-description = Are you sure you want to delete {$profileName}
delete-profile-subtitle = Delete {$profileName}
delete-profile-button = Delete Profile

next-button = Next
previous-button = Previous
reset-button = Reset
download-button = Download

post-date-now-small = now
post-date-seconds-small = {$seconds}s
post-date-minutes-small = {$minutes}m
post-date-hours-small = {$hours}h
post-date-days-small = {$days}d
post-date-weeks-small = {$weeks}w
post-date-years-small = {$years}y

post-date-now = posted now
post-date-seconds = {$seconds ->
    [one] posted one second ago
    *[other] posted {$seconds} seconds ago
    }
post-date-minutes = {$minutes ->
    [one] posted one minute ago
    *[other] posted {$minutes} minutes ago
    }
post-date-hours = {$hours ->
    [one] posted one hour ago
    *[other] posted {$hours} hours ago
    }
post-date-days = {$days ->
    [one] posted one day ago
    *[other] posted {$days} days ago
    }
post-date-weeks = {$weeks ->
    [one] posted one week ago
    *[other] posted {$weeks} weeks ago
    }
post-date-years = {$years ->
    [one] posted one year ago
    *[other] posted {$years} years ago
    }

admin-report-title = Report
admin-report-description = Report {$reportId}
admin-report-heading = Reported Item
admin-report-content = Report Content
admin-report-actions = Actions
admin-report-resolve-select = Resolution
admin-report-ignore = Close Report
admin-report-delete = Delete Offending Item
admin-report-suspend = Suspend Account
admin-report-resolve-input = Resolution Message
admin-report-resolve-placeholder = Explain why you're resolving this report
admin-report-resolve-button = Resolve

admin-settings-title = Admin Settings
admin-settings-subtitle = Perform admin operations for {site-name}
server-info-heading = Server Info
server-info-title-input = Title
server-info-title-placeholder = Name your server
server-info-description-input = Description
server-info-description-placeholder = Describe your server
server-info-submit-button = Save

admin-closed-reports-heading = Closed Reports
admin-reports-heading = Open Reports
admin-reports-reported = reported
admin-reports-prev = Previous
admin-reports-next = Next
author-owned = {$author}'s
admin-reports-submission = submission:
admin-reports-comment = comment:
admin-reports-note = Note:
admin-reports-view-button = View

admin-discover-server-heading = Discover Server
admin-discover-server-description =
    This will attempt to discover the provided server. If it works, the server should appear
    in the "{admin-known-servers-heading}" section after a short time.
admin-discover-input = Discover
admin-discover-placeholder = Enter server URL
admin-discover-button = Discover

admin-inbound-federation-heading = Inbound Federation Requests
admin-outbound-federation-heading = Outbound Federation Requests
admin-federated-servers-heading = Federated Servers
admin-blocked-servers-heading = Blocked Servers
admin-known-servers-heading = Known Servers

admin-federation-federate = Federate
admin-federation-accept = Accept
admin-federation-reject = Reject
admin-federation-cancel = Cancel
admin-federation-defederate = Defederate
admin-federation-block = Block
admin-federation-unblock = Unblock

server-previous-page = Previous
server-next-page = Next

update-comment-heading = Update Comment
update-comment-button = Save

replies-section-heading = Replies

comment-missing-title = Comment Gone
comment-missing-subtitle = Not sure where that comment went!

comment-heading = Comment
comment-input = Comment
comment-placeholder = Comment on this submission
comment-button = Post Comment

reply-heading = Comment
reply-input = Reply
reply-placeholder = Reply to this Comment
reply-button = Post Reply
back-to-submission-button = Back to Submission

comment-section-heading = Comments
replying-to = Replying to {$profileName}
view-link = View
reply-link = Reply
edit-link = Edit

report-comment-heading = Report Comment
report-comment-description = Report {$author}'s comment on {site-name}
report-comment-success-heading = Reported Comment
report-comment-success-description = Reported {$author}'s comment
report-submission-heading = Report {$submissionTitle}
report-submission-description = Report {$author}'s submission on {site-name}
report-submission-success-heading = Reported submission
report-submission-success-subheading = Reported {$author}'s submission
report-profile-heading = Report Profile
report-profile-description = Report {$profileName}'s profile on {site-name}
report-profile-success-heading = Reported Profile
report-profile-success-subheading = Reported {$profileName}'s profile

report-description =
    Please include any relevant information for moderators to act on this report.
report-input = Report
report-placeholder = Type your report info here
back-to-comment-button = Back to Comment
back-to-profile-button = Back to Profile

feed-title = Feed
feed-subtitle = {$profile}'s feed on {site-name}
feed-empty-title = There's nothing here
feed-empty-subtitle = Follow users on Hyaenidae to start seeing submissions in your feed
feed-empty-browse-users = Browse Users
feed-empty-browse-submissions = Browse Submissions

submissions-tab = Submissions
profiles-tab = Users

notification-count = Notifications: {$count}
notification-subtitle = Notifications on {site-name}
notification-clear-all-heading = Clear All
notification-clear-all-description =
    This will clear all notifications except for Follow Requests
notification-clear-button = Clear

follow-requests-heading = Follow Requests
follow-accept = Accept
follow-accept-all = Accept All
follow-reject = Reject
follow-reject-all = Reject All

notification-comment-heading = Comments
notification-commented = commented on your submission:
notification-replied = replied to your
notification-comment-comment = comment
notification-comment-view = View
notification-comment-remove = Remove
notification-comment-clear = Clear All

discover-users-title = Discover Users
discover-users-subtitle = Find users on {site-name}
discover-users-search-button = Search
discover-users-search-placeholder = Search Users by Handle

profile-drafts-subtitle = {$profileName}'s drafts on {site-name}
profile-view-subtitle = {$profileName}'s profile on {site-name}
