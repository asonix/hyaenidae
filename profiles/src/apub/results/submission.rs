use super::{
    LocalSubmissionCreated, LocalSubmissionUpdated, SubmissionCreated, SubmissionDeleted,
    SubmissionUpdated, UnpublishedSubmissionCreated,
};
use crate::{
    apub::{ExtendedNote, Sensitive},
    store::{FileSource, Submission, Visibility},
    Context, Error, OnBehalfOf, Outbound, Required,
};
use activitystreams::{
    activity::{Create, Delete, Update},
    base::AnyBase,
    context,
    object::Image,
    object::{ApObject, Note},
    prelude::*,
    public, security,
};
use std::collections::HashSet;
use url::Url;
use uuid::Uuid;

fn submission_context() -> AnyBase {
    AnyBase::from_arbitrary_json(serde_json::json!({
        "sensitive": "as:sensitive"
    }))
    .unwrap()
}

fn build_image(file_id: Uuid, ctx: &Context) -> Result<AnyBase, Error> {
    if let Some(apub_id) = ctx.apub.apub_for_file(file_id)? {
        ctx.apub.object(&apub_id)?.req("image object by id")
    } else {
        let file = ctx.store.files.by_id(file_id)?.req("file by id")?;

        let FileSource::PictRs(pictrs_file) = file.source();

        let image_id = ctx.apub.info.gen_id().req("id generation")?;
        ctx.apub.file(&image_id, file_id)?;

        let mut image = Image::new();
        image
            .set_id(image_id)
            .set_url(ctx.pictrs.info.image_url(pictrs_file.key()))
            .set_media_type(pictrs_file.media_type().clone());

        let image = image.into_any_base()?;
        ctx.apub.store_object(&image)?;

        Ok(image)
    }
}

fn remote_inboxes(submissioner_id: Uuid, ctx: &Context) -> Result<Vec<Url>, Error> {
    let follower_inboxes = ctx
        .store
        .followers_for(submissioner_id)
        .filter_map(|follower_id| {
            if !ctx.is_local(follower_id).ok()? {
                Some(
                    ctx.apub
                        .endpoints_for_profile(follower_id)
                        .ok()??
                        .inbox()
                        .to_owned(),
                )
            } else {
                None
            }
        })
        .collect::<HashSet<_>>();

    Ok(follower_inboxes.into_iter().collect())
}

fn build_submission(
    submission: Submission,
    note_id: Url,
    person_id: Url,
    ctx: &Context,
) -> Result<AnyBase, Error> {
    let published = submission.published().req("published")?;

    let endpoints = ctx
        .apub
        .endpoints_for_profile(submission.profile_id())?
        .req("endpoints for profile")?;

    let mut note = ExtendedNote::new(
        ApObject::new(Note::new()),
        Sensitive {
            sensitive: submission.is_sensitive(),
        },
    );
    note.set_id(note_id)
        .set_summary(submission.title())
        .set_published(published.into())
        .set_attributed_to(person_id);
    if let Some(updated) = submission.updated() {
        note.set_updated(updated.into());
    }
    if let Some(description) = submission.description() {
        note.set_content(description);
    }
    match submission.visibility() {
        Visibility::Public => {
            note.add_to(public());
            if let Some(followers) = endpoints.followers {
                note.add_cc(followers);
            }
        }
        Visibility::Unlisted => {
            note.add_cc(public());
            if let Some(followers) = endpoints.followers {
                note.add_to(followers);
            }
        }
        Visibility::Followers => {
            if let Some(followers) = endpoints.followers {
                note.add_to(followers);
            }
        }
    }
    for file in submission.files() {
        let image = build_image(*file, ctx)?;
        note.add_attachment(image);
    }
    let any_base = note.into_any_base()?;
    ctx.apub.store_object(&any_base)?;

    Ok(any_base)
}

impl Outbound for UnpublishedSubmissionCreated {
    fn id(&self) -> Option<Uuid> {
        Some(self.submission_id)
    }

    fn ready(&self) -> bool {
        false
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Err(Error::Invalid)
    }

    fn inboxes(&self, _: &Context) -> Result<Vec<Url>, Error> {
        Err(Error::Invalid)
    }

    fn to_apub(&self, _: &Context) -> Result<AnyBase, Error> {
        Err(Error::Invalid)
    }
}

impl Outbound for LocalSubmissionCreated {
    fn id(&self) -> Option<Uuid> {
        Some(self.submission_id)
    }

    fn ready(&self) -> bool {
        false
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Err(Error::Invalid)
    }

    fn inboxes(&self, _: &Context) -> Result<Vec<Url>, Error> {
        Err(Error::Invalid)
    }

    fn to_apub(&self, _: &Context) -> Result<AnyBase, Error> {
        Err(Error::Invalid)
    }
}

impl Outbound for SubmissionCreated {
    fn id(&self) -> Option<Uuid> {
        Some(self.submission_id)
    }

    fn behalf(&self, ctx: &Context) -> Result<OnBehalfOf, Error> {
        let profile_id = ctx
            .store
            .submissions
            .by_id(self.submission_id)?
            .req("submission by id")?
            .profile_id();

        Ok(OnBehalfOf::Profile(profile_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let profile_id = ctx
            .store
            .submissions
            .by_id(self.submission_id)?
            .req("submission by id")?
            .profile_id();

        remote_inboxes(profile_id, ctx)
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let submission = ctx
            .store
            .submissions
            .by_id(self.submission_id)?
            .req("submission by id")?;
        let person_id = ctx
            .apub
            .apub_for_profile(submission.profile_id())?
            .req("apub id for profile")?;
        let note_id = ctx.apub.info.gen_id().req("id generation")?;
        ctx.apub.submission(&note_id, submission.id())?;
        let note = build_submission(submission, note_id, person_id.clone(), ctx)?;

        let mut create = Create::new(person_id, note);
        create
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(submission_context());
        let any_base = create.into_any_base()?;
        ctx.apub.store_object(&any_base)?;

        Ok(any_base)
    }
}

impl Outbound for SubmissionUpdated {
    fn id(&self) -> Option<Uuid> {
        Some(self.submission_id)
    }

    fn behalf(&self, ctx: &Context) -> Result<OnBehalfOf, Error> {
        let profile_id = ctx
            .store
            .submissions
            .by_id(self.submission_id)?
            .req("submission by id")?
            .profile_id();

        Ok(OnBehalfOf::Profile(profile_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let profile_id = ctx
            .store
            .submissions
            .by_id(self.submission_id)?
            .req("submission by id")?
            .profile_id();

        remote_inboxes(profile_id, ctx)
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let submission = ctx
            .store
            .submissions
            .by_id(self.submission_id)?
            .req("submission by id")?;
        let person_id = ctx
            .apub
            .apub_for_profile(submission.profile_id())?
            .req("apub id for profile")?;
        let note_id = ctx
            .apub
            .apub_for_submission(submission.id())?
            .req("apub id for submission")?;
        let note = build_submission(submission, note_id, person_id.clone(), ctx)?;

        let mut update = Update::new(person_id, note);
        update
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(submission_context());
        let any_base = update.into_any_base()?;
        ctx.apub.store_object(&any_base)?;

        Ok(any_base)
    }
}

impl Outbound for LocalSubmissionUpdated {
    fn id(&self) -> Option<Uuid> {
        Some(self.submission_id)
    }

    fn ready(&self) -> bool {
        false
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Err(Error::Invalid)
    }

    fn inboxes(&self, _: &Context) -> Result<Vec<Url>, Error> {
        Err(Error::Invalid)
    }

    fn to_apub(&self, _: &Context) -> Result<AnyBase, Error> {
        Err(Error::Invalid)
    }
}

impl Outbound for SubmissionDeleted {
    fn id(&self) -> Option<Uuid> {
        None
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Profile(self.profile_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        remote_inboxes(self.profile_id, ctx)
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let person_id = ctx
            .apub
            .apub_for_profile(self.profile_id)?
            .req("apub id for profile")?;
        let note_id = ctx
            .apub
            .apub_for_submission(self.submission_id)?
            .req("apub id for submission")?;
        let note = ctx.apub.object(&note_id)?.req("apub for note")?;

        let mut delete = Delete::new(person_id, note);
        delete
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(submission_context());
        let any_base = delete.into_any_base()?;
        ctx.apub.store_object(&any_base)?;

        Ok(any_base)
    }
}
