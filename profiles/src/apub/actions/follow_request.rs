use crate::{
    apub::actions::{
        AcceptFollowRequest, CreateFollowRequest, RejectFollowRequest, UndoFollowRequest,
    },
    Action, Context, Error, Outbound, Required,
};

impl Action for CreateFollowRequest {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        ctx.check_block(self.followed_by_profile, self.followed_profile)?;

        let follow_request = ctx.store.view.follow_requests.new(
            self.followed_profile,
            self.followed_by_profile,
            self.published,
        )?;

        if let Some(apub_id) = &self.follow_apub_id {
            ctx.apub.follow_request(apub_id, follow_request.id)?;
        }

        if let Ok(true) = ctx.is_local(self.followed_profile) {
            ctx.store.view.follow_request_notifs.new(
                self.followed_profile,
                follow_request.id,
                self.published,
            );
        }

        if ctx.is_local(self.followed_by_profile)? && !ctx.is_local(self.followed_profile)? {
            return Ok(Some(Box::new(crate::apub::results::Follow {
                follow_request_id: follow_request.id,
            })));
        }

        Ok(None)
    }
}

impl Action for AcceptFollowRequest {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let opt = ctx
            .store
            .view
            .follow_requests
            .remove(self.follow_request_id)?;

        if let Some(undo_follow_request) = opt {
            ctx.store
                .view
                .follow_request_notifs
                .remove(undo_follow_request.0.id);

            let follow = ctx.store.view.follows.new(
                undo_follow_request.0.left,
                undo_follow_request.0.right,
                self.published,
            )?;

            if let Some(apub_id) = &self.accept_apub_id {
                ctx.apub.follow(apub_id, follow.id)?;
            }

            if ctx.is_local(follow.left)? && !ctx.is_local(follow.right)? {
                let follow_apub_id = ctx
                    .apub
                    .apub_for_follow_request(undo_follow_request.0.id)?
                    .req("apub for follow request")?;
                return Ok(Some(Box::new(crate::apub::results::AcceptFollow {
                    follow_apub_id,
                    profile_id: follow.left,
                    follow_id: follow.id,
                })));
            }
        }

        Ok(None)
    }
}

impl Action for RejectFollowRequest {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let opt = ctx
            .store
            .view
            .follow_requests
            .remove(self.follow_request_id)?;

        if let Some(undo_follow_request) = opt {
            ctx.store
                .view
                .follow_request_notifs
                .remove(undo_follow_request.0.id);

            if let Some(follow_apub_id) =
                ctx.apub.apub_for_follow_request(self.follow_request_id)?
            {
                ctx.apub.delete_object(&follow_apub_id)?;

                if ctx.is_local(undo_follow_request.0.left)?
                    && !ctx.is_local(undo_follow_request.0.right)?
                {
                    return Ok(Some(Box::new(crate::apub::results::RejectFollow {
                        follow_apub_id,
                        profile_id: undo_follow_request.0.left,
                        requester_id: undo_follow_request.0.right,
                    })));
                }
            }
        }

        Ok(None)
    }
}

impl Action for UndoFollowRequest {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let opt = ctx
            .store
            .view
            .follow_requests
            .remove(self.follow_request_id)?;

        if let Some(undo_follow_request) = opt {
            ctx.store
                .view
                .follow_request_notifs
                .remove(undo_follow_request.0.id);

            if let Some(follow_apub_id) =
                ctx.apub.apub_for_follow_request(self.follow_request_id)?
            {
                ctx.apub.delete_object(&follow_apub_id)?;

                if ctx.is_local(undo_follow_request.0.right)?
                    && !ctx.is_local(undo_follow_request.0.left)?
                {
                    return Ok(Some(Box::new(crate::apub::results::UndoFollow {
                        follow_apub_id,
                        profile_id: undo_follow_request.0.right,
                        followed_id: undo_follow_request.0.left,
                    })));
                }
            }
        }

        Ok(None)
    }
}
