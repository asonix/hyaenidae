use super::{Block as Blocked, UndoBlock};
use crate::{Context, Error, OnBehalfOf, Outbound, Required};
use activitystreams::{
    activity::{Block, Undo},
    base::AnyBase,
    context,
    prelude::*,
    security,
};
use url::Url;
use uuid::Uuid;

impl Outbound for Blocked {
    fn id(&self) -> Option<Uuid> {
        Some(self.block_id)
    }

    fn behalf(&self, ctx: &Context) -> Result<OnBehalfOf, Error> {
        let blocker_id = ctx
            .store
            .view
            .blocks
            .right(self.block_id)?
            .req("blocker profile by block id")?;

        Ok(OnBehalfOf::Profile(blocker_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let blocked_id = ctx
            .store
            .view
            .blocks
            .left(self.block_id)?
            .req("blocked profile by block id")?;
        let inbox = ctx
            .apub
            .endpoints_for_profile(blocked_id)?
            .req("endpoints for blocked profile")?
            .inbox()
            .to_owned();

        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let block = ctx
            .store
            .view
            .blocks
            .by_id(self.block_id)?
            .req("block by id")?;
        let actor_id = ctx
            .apub
            .apub_for_profile(block.right)?
            .req("apub for blocking profile")?;
        let object_id = ctx
            .apub
            .apub_for_profile(block.left)?
            .req("apub for blocked profile")?;
        let published = block.published;

        let mut block = Block::new(actor_id, object_id);
        block
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .set_published(published.into())
            .add_context(context())
            .add_context(security());
        let block = block.into_any_base()?;
        ctx.apub.store_object(&block)?;

        Ok(block)
    }
}

impl Outbound for UndoBlock {
    fn id(&self) -> Option<Uuid> {
        None
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Profile(self.profile_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let inbox = ctx
            .apub
            .endpoints_for_profile(self.blocked_id)?
            .req("endpoints for blocked profile")?
            .inbox()
            .to_owned();
        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let person_id = ctx
            .apub
            .apub_for_profile(self.profile_id)?
            .req("apub for blocking profile")?;
        let block = ctx
            .apub
            .object(&self.block_apub_id)?
            .req("apub for block")?;

        let mut undo = Undo::new(person_id, block);
        undo.set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security());
        let undo = undo.into_any_base()?;
        ctx.apub.store_object(&undo)?;

        Ok(undo)
    }
}
