use crate::{
    apub::actions::{
        CreateServerBlock, DeleteServerBlock, RejectFederationRequest, UndoAcceptFederation,
        UndoFederation, UndoFederationRequest,
    },
    Action, Context, Error, Outbound,
};

impl Action for CreateServerBlock {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        ctx.store.view.server_blocks.new(
            self.blocked_server,
            self.blocked_by_server,
            self.published,
        )?;

        if let Ok(Some(follow_request_id)) = ctx
            .store
            .view
            .server_follow_requests
            .by_forward(self.blocked_server, self.blocked_by_server)
        {
            Action::perform(&UndoFederationRequest { follow_request_id }, ctx)?;
        }

        if let Ok(Some(follow_request_id)) = ctx
            .store
            .view
            .server_follow_requests
            .by_forward(self.blocked_by_server, self.blocked_server)
        {
            Action::perform(&RejectFederationRequest { follow_request_id }, ctx)?;
        }

        if let Ok(Some(follow_id)) = ctx
            .store
            .view
            .server_follows
            .by_forward(self.blocked_server, self.blocked_by_server)
        {
            Action::perform(&UndoFederation { follow_id }, ctx)?;
        }

        if let Ok(Some(follow_id)) = ctx
            .store
            .view
            .server_follows
            .by_forward(self.blocked_by_server, self.blocked_server)
        {
            Action::perform(&UndoAcceptFederation { follow_id }, ctx)?;
        }

        Ok(None)
    }
}

impl Action for DeleteServerBlock {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        ctx.store.view.server_blocks.remove(self.block_id)?;

        Ok(None)
    }
}
