use combine::{
    choice, count, many1,
    parser::char::{char as parsechar, hex_digit, lower},
    Parser, Stream,
};

pub(crate) fn color<Input>() -> impl Parser<Input, Output = String>
where
    Input: Stream<Token = char>,
{
    let hashcolor = parsechar('#')
        .with(count(8, hex_digit()))
        .map(|color: String| format!("#{}", color));

    let namecolor = many1(lower());

    choice((hashcolor, namecolor))
}

#[cfg(test)]
mod tests {
    use super::*;
    use combine::EasyParser;

    #[test]
    fn parse_shortcolor() {
        let (_, rest) = color().easy_parse("#aaa").unwrap();
        assert_eq!(rest, "");
    }

    #[test]
    fn parse_longcolor() {
        let (_, rest) = color().easy_parse("#aaaaaaff").unwrap();
        assert_eq!(rest, "");
    }

    #[test]
    fn parse_colorname() {
        let (value, rest) = color().easy_parse("white").unwrap();
        assert_eq!(rest, "");
        assert_eq!(value, "white");
    }
}
