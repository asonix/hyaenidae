use crate::{
    apub::{
        actions::{
            apub::require_not_blocked, CreateServer, CreateServerApub, DeleteServer, UpdateServer,
            UpdateServerApub,
        },
        Endpoints, ExtendedApplication,
    },
    recover, Action, Context, Error, KeyOwner, RecoverableError, Required,
};
use activitystreams::prelude::*;
use chrono::Utc;

pub mod follow;

pub(crate) fn application(
    application: &ExtendedApplication,
    key_owner: Option<KeyOwner>,
    ctx: &Context,
) -> Result<Result<Box<dyn Action>, RecoverableError>, Error> {
    let id = application.id_unchecked().req("application id")?;
    require_not_blocked(key_owner, id, ctx)?;

    // Double create
    let server_id = ctx.apub.id_for_apub(id)?;

    let handle = application
        .preferred_username()
        .req("preferred username")?
        .to_owned();
    let domain = id.domain().req("application id's domain")?.to_owned();

    if handle != domain {
        log::warn!("Received message from application where domain != handle");
        return Err(Error::Invalid);
    }

    let inbox = application.inbox()?.clone();
    let outbox = application.outbox()?.map(|o| o.to_owned());
    let following = application.following()?.map(|f| f.to_owned());
    let followers = application.followers()?.map(|f| f.to_owned());
    let shared_inbox = application
        .endpoints()?
        .and_then(|e| e.shared_inbox.map(|s| s.to_owned()));
    let title = application
        .name()
        .and_then(|n| n.one())
        .and_then(|s| s.as_xsd_string())
        .map(|s| s.to_owned());
    let description = application
        .summary()
        .and_then(|s| s.as_single_xsd_string().map(|s| s.to_owned()));
    let public_key = application.ext_one.public_key.public_key_pem.clone();
    let public_key_id = application.ext_one.public_key.id.clone();
    let published = application
        .published()
        .map(|published| published.into())
        .unwrap_or_else(|| Utc::now());
    let updated = application.updated().map(|updated| updated.into());

    let endpoints = Endpoints {
        inbox,
        outbox,
        following,
        followers,
        shared_inbox,
        public_key: public_key_id,
    };

    if let Some(server_id) = server_id.and_then(|sid| sid.server()) {
        Ok(Ok(Box::new(UpdateServer {
            apub: Some(UpdateServerApub {
                public_key,
                endpoints,
            }),
            server_id,
            title,
            title_source: None,
            description,
            description_source: None,
            updated: updated.req("updated")?,
        })))
    } else {
        Ok(Ok(Box::new(CreateServer {
            apub: Some(CreateServerApub {
                application_apub_id: id.to_owned(),
                public_key,
                endpoints,
            }),
            domain,
            title,
            description,
            published,
            updated,
        })))
    }
}

pub(crate) fn create_application(
    create: &activitystreams::activity::Create,
    application_obj: &ExtendedApplication,
    key_owner: Option<KeyOwner>,
    ctx: &Context,
) -> Result<Result<Box<dyn Action>, RecoverableError>, Error> {
    let actor_id = create.actor()?.as_single_id().req("create actor id")?;
    require_not_blocked(key_owner, actor_id, ctx)?;

    let application_id = application_obj.id_unchecked().req("application id")?;

    if actor_id != application_id {
        return Err(Error::Invalid);
    }

    application(application_obj, None, ctx)
}

pub(crate) fn update_application(
    update: &activitystreams::activity::Update,
    application: &ExtendedApplication,
    key_owner: Option<KeyOwner>,
    ctx: &Context,
) -> Result<Result<Box<dyn Action>, RecoverableError>, Error> {
    let update_actor = update.actor()?.as_single_id().req("update actor id")?;
    require_not_blocked(key_owner, update_actor, ctx)?;

    let id = application.id_unchecked().req("application id")?;

    if update_actor != id {
        return Err(Error::Invalid);
    }

    let handle = application
        .preferred_username()
        .req("preferred username")?
        .to_owned();
    let domain = id.domain().req("domain from id")?.to_owned();

    if handle != domain {
        return Err(Error::Invalid);
    }

    let server_id = recover!(id, ctx.apub.id_for_apub(id)?);
    let server_id = server_id.server().req("application id as server id")?;

    let inbox = application.inbox()?.clone();
    let outbox = application.outbox()?.map(|o| o.to_owned());
    let following = application.following()?.map(|f| f.to_owned());
    let followers = application.followers()?.map(|f| f.to_owned());
    let shared_inbox = application
        .endpoints()?
        .and_then(|e| e.shared_inbox.map(|s| s.to_owned()));
    let title = application
        .name()
        .and_then(|n| n.one())
        .and_then(|s| s.as_xsd_string())
        .map(|s| s.to_owned());
    let description = application
        .summary()
        .and_then(|s| s.as_single_xsd_string().map(|s| s.to_owned()));
    let public_key_id = application.ext_one.public_key.id.clone();
    let public_key = application.ext_one.public_key.public_key_pem.clone();
    let updated = application.updated().req("updated")?.into();

    let endpoints = Endpoints {
        inbox,
        outbox,
        following,
        followers,
        shared_inbox,
        public_key: public_key_id,
    };

    Ok(Ok(Box::new(UpdateServer {
        apub: Some(UpdateServerApub {
            endpoints,
            public_key,
        }),
        server_id,
        title,
        title_source: None,
        description,
        description_source: None,
        updated,
    })))
}

pub(crate) fn delete_application(
    delete: &activitystreams::activity::Delete,
    application: &ExtendedApplication,
    key_owner: Option<KeyOwner>,
    ctx: &Context,
) -> Result<Result<Box<dyn Action>, RecoverableError>, Error> {
    let delete_actor = delete.actor()?.as_single_id().req("delete actor id")?;
    require_not_blocked(key_owner, delete_actor, ctx)?;

    let id = application.id_unchecked().req("application id")?;

    if delete_actor != id {
        return Err(Error::Invalid);
    }

    let server_id = recover!(id, ctx.apub.id_for_apub(id)?);
    let server_id = server_id.server().req("application id as server id")?;

    Ok(Ok(Box::new(DeleteServer { server_id })))
}
