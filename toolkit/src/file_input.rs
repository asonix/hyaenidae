use crate::ButtonKind;

#[derive(Debug, Default)]
pub struct FileInput {
    pub(crate) name: String,
    pub(crate) label: String,
    pub(crate) kind: ButtonKind,
    pub(crate) accept: String,
    pub(crate) classes: Vec<String>,
    pub(crate) multiple: bool,
    pub(crate) group: bool,
    dark: bool,
}

impl FileInput {
    pub fn primary(name: &str, label: &str) -> Self {
        FileInput {
            name: name.to_owned(),
            label: label.to_owned(),
            group: true,
            ..Default::default()
        }
    }

    pub fn primary_outline(name: &str, label: &str) -> Self {
        FileInput {
            name: name.to_owned(),
            label: label.to_owned(),
            kind: ButtonKind::PrimaryOutline,
            group: true,
            ..Default::default()
        }
    }

    pub fn outline(name: &str, label: &str) -> Self {
        FileInput {
            name: name.to_owned(),
            label: label.to_owned(),
            kind: ButtonKind::Outline,
            group: true,
            ..Default::default()
        }
    }

    pub fn secondary(name: &str, label: &str) -> Self {
        FileInput {
            name: name.to_owned(),
            label: label.to_owned(),
            kind: ButtonKind::Secondary,
            group: true,
            ..Default::default()
        }
    }

    pub fn multiple(mut self) -> Self {
        self.multiple = true;
        self
    }

    pub fn classes(mut self, classes: &[&str]) -> Self {
        self.classes = classes.into_iter().map(|s| s.to_string()).collect();
        self
    }

    pub fn accept(mut self, accept: &str) -> Self {
        self.accept = accept.to_string();
        self
    }

    pub fn no_group(mut self) -> Self {
        self.group = false;
        self
    }

    pub fn dark(mut self, dark: bool) -> Self {
        self.dark = dark;
        self
    }

    pub(crate) fn class_string(&self) -> String {
        use ButtonKind::*;

        let mut classes = self.classes.clone();

        classes.push("toolkit-file".to_owned());

        let static_classes = match self.kind {
            Primary => "toolkit-file__primary",
            PrimaryOutline => "toolkit-file__primary-outline",
            PrimaryLink => "toolkit-file__link",
            Secondary => "toolkit-file__secondary",
            Outline => "toolkit-file__outline",
            Link => "toolkit-file__link",
        };

        classes.push(static_classes.to_owned());

        if self.dark {
            classes.push("toolkit-dark".to_owned());
        }

        classes.join(" ")
    }
}
