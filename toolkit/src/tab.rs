#[derive(Clone, Debug, Default)]
pub struct Tab {
    href: String,
    selected: bool,
}

impl Tab {
    pub fn new(href: &str) -> Self {
        Tab {
            href: href.to_owned(),
            selected: false,
        }
    }

    pub fn selected(mut self, selected: bool) -> Self {
        self.selected = selected;
        self
    }

    pub(crate) fn href(&self) -> &str {
        &self.href
    }

    pub(crate) fn class_string(&self) -> String {
        let mut classes = vec!["toolkit-tab".to_owned()];

        if self.selected {
            classes.push("toolkit-tab__selected".to_owned());
        }

        classes.join(" ")
    }
}
