#[derive(Clone, Copy, Debug)]
pub enum InputKind {
    Text,
    TextArea,
    Number,
    Password,
}

#[derive(Debug, Default)]
pub struct Input {
    pub(crate) name: String,
    pub(crate) kind: InputKind,
    pub(crate) value: Option<String>,
    pub(crate) placeholder: Option<String>,
    classes: Vec<String>,
    dark: bool,
}

impl Input {
    pub fn new(name: &str, kind: InputKind) -> Self {
        Input {
            name: name.to_owned(),
            kind,
            ..Default::default()
        }
    }

    pub fn kind(mut self, kind: InputKind) -> Self {
        self.kind = kind;
        self
    }

    pub fn value(mut self, value: &str) -> Self {
        self.value = Some(value.to_owned());
        self
    }

    pub fn placeholder(mut self, placeholder: &str) -> Self {
        self.placeholder = Some(placeholder.to_owned());
        self
    }

    pub fn value_opt(mut self, value: Option<String>) -> Self {
        self.value = value;
        self
    }

    pub fn classes(mut self, classes: &[&str]) -> Self {
        self.classes = classes.into_iter().map(|s| s.to_string()).collect();
        self
    }

    pub fn dark(mut self, dark: bool) -> Self {
        self.dark = dark;
        self
    }

    pub(crate) fn is_textarea(&self) -> bool {
        match self.kind {
            InputKind::TextArea => true,
            _ => false,
        }
    }

    pub(crate) fn kind_str(&self) -> &'static str {
        use InputKind::*;
        match self.kind {
            Text => "text",
            TextArea => "textarea",
            Number => "number",
            Password => "password",
        }
    }

    pub(crate) fn class_string(&self) -> String {
        let mut classes = self.classes.clone();

        classes.push("toolkit-input--input".to_owned());

        if self.dark {
            classes.push("toolkit-dark".to_owned());
        }

        classes.join(" ")
    }
}

impl Default for InputKind {
    fn default() -> Self {
        InputKind::Text
    }
}
