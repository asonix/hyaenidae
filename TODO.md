# Things That Need To Be Done

## Actix Web 4.0
It's not out yet, and not everything has a beta release yet, but we should continue tracking the
status of 4.0 and update our libraries as we can.

Current status:
- background-jobs on actix-rt 2.0.2
- actix-fs on actix-rt 2.0.2
- actix-webfinger on actix-web 4.0.0-beta.3
- http-signature-normalization-actix on actix-web 4.0.0-beta.3
- actix-form-data on actix-web 4.0.0-beta.3
- pict-rs on actix-web 4.0.0-beta.3
- relay on actix-web 4.0.0-beta.3

waiting on an actix-session release to start updating hyaenidae

## Sled 1.0
As of the time of writing, the official Sled 1.0 release date is ~~in two days~~ eventually. I'm
not sure how much the API will change, but I can assume there will be changes around flushing and
transactions, as those have been the subjects of recent PRs against Sled. Most of the other PRs
against Sled in the last month or so have been about reducing memory usage by employing a variety
of fancy techniques backed by whitepapers.

## Async
A lot of logic in the Profiles and Submissions modules synchronously accesses Sled, which is an
issue because Sled is potentially blocking. All of the Big Dangers (transactions) are safely on a
threadpool already, but a couple iterations are currently occuring directly on the Arbiters.

## Organization
A lot of modules in hyaenidae-server have circular dependencies, and a few are more than a thousand
lines long, each. These should be separated where possible into smaller modules which create a
dependency DAG

## Validation
Right now, a lot of the validation happens in multiple places in the codebase, which is not great. I
think there should be a Single Way we do validation in order to keep ourselves sane in the future
when we're trying to debug stuff

Tasks:
- Enable validation on the [Thing]Changes structs
    - ReactChanges
- Distinguish between local & federated when validation maxium lengths
- Make a global maximum length that things get truncated to if they're too long
- Expose validation failures in the user interface

## Pagination
Tasks:
- Enable pagination for notifications
- (potentially) Enable pagination for comments on submissions
    - this one is less clear because comments are tree-structures

## Submissions
Tasks:
- Add "Mark as Sensitive" to report actions
- Ensure submission updates cannot override an administrator's "mark as sensitive" ruling
- (potentially) expose an NSFW flag on profiles to both hide profile banner image, and to default
    submissions to NSFW

## Registration
There is currently no way to disable or limit registration to Hyaenidae

Tasks:
- Add an option in the Admin settings to disable registration
- Create a concept of Invite Links which can be used N (user specified) times to register accounts
    when registration is disabled
- Add an option in the Admin settings to generate invite links

## Tags
Tags are useful for searching Hyaenidae for content. There should be a tag system that makes it
simple to search for submissions.

Tasks:
- Design a Tag database optimized for multi-tag lookups (within reason)
- Design a UI for authors to add tags to a submission
- Design ActivityPub interop for tag federation

## Reports
Reports are implemented for on-server moderation, but cross-server moderation has not been tested.

Tasks:
- Add a "delete" mechanism for remote objects. This should not induce any federation, but should
    ensure the object is not accessible locally
- Enable forwarding reports to origin server. There is already code written to perform this
    forwarding, but it is not exposed in the UI.
- Enable viewing of closed reports

## Reacts
Rethink whether we actually want to do reacts. It may be that just having Likes is fine
Reacts are similar to "likes" on other platforms, but do not get added to a collection for a given
profile. Reacts only notify the author of the item being reacted to.

Tasks:
- Design a UI for reacting to submissions
- Design a UI for reacting to comments
- Design a UI for reacting to posts (see Posts)
- Decide which kinds of reacts are valid

## Posts
Posts are top-level content created by profiles that do not have associated images, and so cannot be
considered Submissions. Posts are stored and viewed separately from submissions, and are more
"ephemeral" in that they will not backfill (see Backfills).

Tasks:
- Design a Post database
- Design a UI for creating new posts
- Design a UI for viewing posts from followed accounts
- Hook Posts into Note federation code

## Direct Messages
Direct Messages are Posts that are shared directly between two profiles, and are not publically
visible to other profiles.

## Shares
Shares are a collection of submissions a profile can display that were submitted by other profiles.
They will not appear in followers' feeds, but will be visible on a given profile's page. This
feature is analogous to a public favorites gallery on other platforms, although it is separate from
"reacts", which can act as favoriting a post. Whether a profile is notified of a share is undecided.

Tasks:
- Add shares tree(s) in the data store. This is likely not much more than a single tree with a key
    consisting of the Profile ID, a timestamp, and the Submission ID, where the value of is the
    Submission ID as well. A second tree could contain Profile ID + Submission ID keys for quick
    lookups for whether a given submission has been shared by a given profile to avoid duplicates
- Add a shares page or section to the profile section of Hyaenidae.
- Implement Shares in ActivityPub as a variant of Announce Note where the Note represents a
    Submission

## Backfills
Bakfills for a given item should not be permitted to run concurrently. A single backfill task may be
permitted to have multiple requests in-flight, but no two tasks may backfill the same item. There
should also be a limit on concurrency for backfills from a given server

Tasks:
- Add Profile Backfill on federation
- Add Submission Backfill on follow

## Object Cleanup
When things get deleted or suspended, we need to ensure that the database does not hold onto any
lingering data. This includes cleaning text fields for deleted items, cleaning up dependent items on
deletion.

Tasks:
- Differentiate between Profile Deletion and Profile Suspsension
- Federate profile suspensions for local suspended profiles
- Delete profiles (and submissions and comments and reacts) for defederated servers

## Federation Fixes
Not all of federation has been tested yet

Things that have been tested:
- Federation:
    - Request
    - Accept
    - Reject
    - Defederate
- Profiles:
    - Create
    - Update
- Submissions:
    - Create
    - Update

Anything not in this list has not been tested and needs to be verified and possibly fixed.

## Blind Key Rotation
If an incoming post has an invalid key, we should try fetching the key/profile & then verifying the
signature again. This is because other softwares, like mastodon, work on the assumption that you
might not want deleted posts to be associated with your account. It's not a Big Deal IMO when only
HTTP Signatures are in use, such as in Hyaenidae, but when JSON-LD Signatures are in use by other
software, it becomes trivial to trace authorship of any object.
