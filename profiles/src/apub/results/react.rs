use super::{React, UndoReact};
use crate::{Context, Error, OnBehalfOf, Outbound, Required};
use activitystreams::{
    activity::{Create, Delete, Like},
    base::AnyBase,
    context,
    prelude::*,
    public, security,
};
use url::Url;
use uuid::Uuid;

impl Outbound for React {
    fn id(&self) -> Option<Uuid> {
        Some(self.react_id)
    }

    fn behalf(&self, ctx: &Context) -> Result<OnBehalfOf, Error> {
        let react = ctx.store.reacts.by_id(self.react_id)?.req("react by id")?;

        Ok(OnBehalfOf::Profile(react.profile_id()))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let react = ctx.store.reacts.by_id(self.react_id)?.req("react by id")?;

        let notifier_id = if let Some(comment_id) = react.comment_id() {
            ctx.store
                .comments
                .by_id(comment_id)?
                .req("comment by id")?
                .profile_id()
        } else {
            ctx.store
                .submissions
                .by_id(react.submission_id())?
                .req("submission by id")?
                .profile_id()
        };

        let inbox = ctx
            .apub
            .endpoints_for_profile(notifier_id)?
            .req("endpoints for profile")?
            .inbox()
            .to_owned();

        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let react = ctx.store.reacts.by_id(self.react_id)?.req("react by id")?;
        let person_id = ctx
            .apub
            .apub_for_profile(react.profile_id())?
            .req("apub id for profile")?;

        let note_id = if let Some(comment_id) = react.comment_id() {
            ctx.apub
                .apub_for_comment(comment_id)?
                .req("apub id for comment")?
        } else {
            ctx.apub
                .apub_for_submission(react.submission_id())?
                .req("apub id for submission")?
        };

        let published = react.published();
        let endpoints = ctx
            .apub
            .endpoints_for_profile(react.profile_id())?
            .req("endpoints for profile")?;

        let mut like = Like::new(person_id.clone(), note_id);
        like.set_id(ctx.apub.info.gen_id().req("id generation")?)
            .set_content(react.react())
            .set_published(published.into())
            .set_attributed_to(person_id.clone())
            .add_cc(public());

        if let Some(followers) = endpoints.followers {
            like.add_to(followers);
        }

        let like = like.into_any_base()?;
        ctx.apub.store_object(&like)?;

        let mut create = Create::new(person_id, like);
        create
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security());

        let create = create.into_any_base()?;
        ctx.apub.store_object(&create)?;

        Ok(create)
    }
}

impl Outbound for UndoReact {
    fn id(&self) -> Option<Uuid> {
        None
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Profile(self.profile_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let notifier_id = if let Some(comment_id) = self.comment_id {
            ctx.store
                .comments
                .by_id(comment_id)?
                .req("comment by id")?
                .profile_id()
        } else {
            ctx.store
                .submissions
                .by_id(self.submission_id)?
                .req("submission by id")?
                .profile_id()
        };

        let inbox = ctx
            .apub
            .endpoints_for_profile(notifier_id)?
            .req("endpoints for profile")?
            .inbox()
            .to_owned();

        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let person_id = ctx
            .apub
            .apub_for_profile(self.profile_id)?
            .req("apub id for profile")?;
        let like = ctx.apub.object(&self.like_apub_id)?.req("apub for like")?;

        let mut delete = Delete::new(person_id, like);
        delete
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security());
        let delete = delete.into_any_base()?;
        ctx.apub.store_object(&delete)?;

        Ok(delete)
    }
}
