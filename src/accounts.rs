use crate::{nav::NavState, rendered, ActixLoader, Error};
use actix_web::{http::header::LOCATION, web, HttpResponse, Scope};
use hyaenidae_accounts::{
    CookiesArgs, CookiesPageArgs, DeleteUserArgs, DeleteUserPageArgs, LoginArgs, LoginPageArgs,
    LogoutArgs, Pages as _, RegisterArgs, RegisterPageArgs, UpdatePasswordArgs,
    UpdatePasswordPageArgs, UpdateUsernameArgs, UpdateUsernamePageArgs, User,
};

pub(crate) struct Pages;

impl hyaenidae_accounts::Pages for Pages {
    fn home_path(&self) -> String {
        "/".to_owned()
    }

    fn not_found_path(&self) -> String {
        "/404".to_owned()
    }

    fn internal_server_error_path(&self) -> String {
        "/500".to_owned()
    }

    fn cookies_path(&self) -> String {
        "/session/cookies".to_owned()
    }

    fn login_path(&self) -> String {
        "/session/auth/login".to_owned()
    }

    fn register_path(&self) -> String {
        "/session/auth/register".to_owned()
    }

    fn logout_path(&self) -> String {
        "/session/auth/logout".to_owned()
    }

    fn accounts_path(&self) -> String {
        "/session/account".to_owned()
    }

    fn update_username_path(&self) -> String {
        "/session/account/username".to_owned()
    }

    fn update_password_path(&self) -> String {
        "/session/account/password".to_owned()
    }

    fn delete_user_path(&self) -> String {
        "/session/account/delete".to_owned()
    }
}

pub(crate) fn scope() -> Scope {
    web::scope("/session")
        .service(
            web::resource("/cookies")
                .route(web::get().to(cookies_page))
                .route(web::post().to(cookies)),
        )
        .service(
            web::scope("/auth")
                .service(
                    web::resource("/login")
                        .route(web::get().to(login_page))
                        .route(web::post().to(login)),
                )
                .service(
                    web::resource("/register")
                        .route(web::get().to(register_page))
                        .route(web::post().to(register)),
                )
                .service(web::resource("/logout").route(web::post().to(logout))),
        )
        .service(
            web::scope("/account")
                .service(web::resource("").route(web::get().to(account_page)))
                .service(
                    web::resource("/username")
                        .route(web::get().to(to_account))
                        .route(web::post().to(update_username)),
                )
                .service(
                    web::resource("/password")
                        .route(web::get().to(to_account))
                        .route(web::post().to(update_password)),
                )
                .service(
                    web::resource("/delete")
                        .route(web::get().to(delete_account_page))
                        .route(web::post().to(delete_account)),
                ),
        )
}

async fn cookies_page(
    loader: ActixLoader,
    args: CookiesPageArgs,
    nav_state: NavState,
) -> Result<HttpResponse, Error> {
    let cookie_state = match hyaenidae_accounts::cookies_page(args) {
        Ok(state) => state,
        Err(res) => return Ok(res),
    };

    rendered(HttpResponse::Ok(), |cursor| {
        crate::templates::session::cookies(cursor, &loader, &cookie_state, &nav_state)
    })
}

async fn cookies(args: CookiesArgs) -> Result<HttpResponse, Error> {
    Ok(hyaenidae_accounts::cookies(args).await?)
}

async fn login_page(
    loader: ActixLoader,
    args: LoginPageArgs,
    nav_state: NavState,
) -> Result<HttpResponse, Error> {
    let login_state = match hyaenidae_accounts::login_page(args) {
        Ok(state) => state,
        Err(res) => return Ok(res),
    };

    rendered(HttpResponse::Ok(), |cursor| {
        crate::templates::session::login(cursor, &loader, &login_state, &nav_state)
    })
}

async fn login(
    loader: ActixLoader,
    args: LoginArgs,
    nav_state: NavState,
) -> Result<HttpResponse, Error> {
    let login_state = match hyaenidae_accounts::login(args).await? {
        Ok(state) => state,
        Err(res) => return Ok(res),
    };

    rendered(HttpResponse::BadRequest(), |cursor| {
        crate::templates::session::login(cursor, &loader, &login_state, &nav_state)
    })
}

async fn register_page(
    loader: ActixLoader,
    args: RegisterPageArgs,
    nav_state: NavState,
) -> Result<HttpResponse, Error> {
    let register_state = match hyaenidae_accounts::register_page(args) {
        Ok(state) => state,
        Err(res) => return Ok(res),
    };

    rendered(HttpResponse::Ok(), |cursor| {
        crate::templates::session::register(cursor, &loader, &register_state, &nav_state)
    })
}

async fn register(
    loader: ActixLoader,
    args: RegisterArgs,
    nav_state: NavState,
) -> Result<HttpResponse, Error> {
    let register_state = match hyaenidae_accounts::register(args).await? {
        Ok(state) => state,
        Err(res) => return Ok(res),
    };

    rendered(HttpResponse::BadRequest(), |cursor| {
        crate::templates::session::register(cursor, &loader, &register_state, &nav_state)
    })
}

async fn logout(args: LogoutArgs) -> HttpResponse {
    hyaenidae_accounts::logout(args)
}

async fn account_page(
    loader: ActixLoader,
    uname_args: UpdateUsernamePageArgs,
    pass_args: UpdatePasswordPageArgs,
    user: User,
    nav_state: NavState,
) -> Result<HttpResponse, Error> {
    let uname_state = hyaenidae_accounts::update_username_page(uname_args);
    let pass_state = hyaenidae_accounts::update_password_page(pass_args);

    rendered(HttpResponse::Ok(), |cursor| {
        crate::templates::session::account(
            cursor,
            &loader,
            &user,
            &uname_state,
            &pass_state,
            &nav_state,
        )
    })
}

async fn to_account() -> HttpResponse {
    HttpResponse::SeeOther()
        .insert_header((LOCATION, Pages.accounts_path()))
        .finish()
}

async fn update_username(
    loader: ActixLoader,
    uname_args: UpdateUsernameArgs,
    pass_args: UpdatePasswordPageArgs,
    user: User,
    nav_state: NavState,
) -> Result<HttpResponse, Error> {
    let uname_state = match hyaenidae_accounts::update_username(uname_args).await? {
        Ok(state) => state,
        Err(res) => return Ok(res),
    };
    let pass_state = hyaenidae_accounts::update_password_page(pass_args);

    rendered(HttpResponse::BadRequest(), |cursor| {
        crate::templates::session::account(
            cursor,
            &loader,
            &user,
            &uname_state,
            &pass_state,
            &nav_state,
        )
    })
}

async fn update_password(
    loader: ActixLoader,
    uname_args: UpdateUsernamePageArgs,
    pass_args: UpdatePasswordArgs,
    user: User,
    nav_state: NavState,
) -> Result<HttpResponse, Error> {
    let uname_state = hyaenidae_accounts::update_username_page(uname_args);
    let pass_state = match hyaenidae_accounts::update_password(pass_args).await? {
        Ok(state) => state,
        Err(res) => return Ok(res),
    };

    rendered(HttpResponse::BadRequest(), |cursor| {
        crate::templates::session::account(
            cursor,
            &loader,
            &user,
            &uname_state,
            &pass_state,
            &nav_state,
        )
    })
}

async fn delete_account_page(
    loader: ActixLoader,
    args: DeleteUserPageArgs,
    nav_state: NavState,
) -> Result<HttpResponse, Error> {
    let delete_state = match hyaenidae_accounts::delete_user_page(args) {
        Ok(state) => state,
        Err(res) => return Ok(res),
    };

    rendered(HttpResponse::Ok(), |cursor| {
        crate::templates::session::delete_account(cursor, &loader, &delete_state, &nav_state)
    })
}

async fn delete_account(
    loader: ActixLoader,
    args: DeleteUserArgs,
    nav_state: NavState,
) -> Result<HttpResponse, Error> {
    let delete_state = match hyaenidae_accounts::delete_user(args).await? {
        Ok(state) => state,
        Err(res) => return Ok(res),
    };

    rendered(HttpResponse::BadRequest(), |cursor| {
        crate::templates::session::delete_account(cursor, &loader, &delete_state, &nav_state)
    })
}
