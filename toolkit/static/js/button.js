(function(fn) {
    if (document.readyState === "complete" || document.readyState === "interactive") {
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
})(function() {
    function preventDoubleClick(event) {
        event.preventDefault();
    }

    function onClick(button, span) {
        return function _listener() {
            span.textContent = "";
            span.setAttribute("class", "fa fa-spinner fa-spin fa-pulse");

            button.removeEventListener("click", _listener);
            button.addEventListener("click", preventDoubleClick);
        };
    }

    var buttons = document.getElementsByClassName("toolkit-button");

    for (var i = 0; i < buttons.length; i++) {
        var span = buttons[i].getElementsByTagName("span")[0];
        var button = buttons[i].getElementsByTagName("button")[0];

        if (!span || !button) {
            continue;
        }

        button.addEventListener("click", onClick(button, span));
    }
})
