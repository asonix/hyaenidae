#[derive(Debug, Default)]
pub struct Card {
    pub(crate) full_width: bool,
    pub(crate) classes: Vec<String>,
    dark: bool,
}

impl Card {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn full_width() -> Self {
        Card {
            full_width: true,
            ..Default::default()
        }
    }

    pub fn centered(mut self) -> Self {
        self.classes.push("toolkit-centered".to_owned());
        self
    }

    pub fn classes(mut self, classes: &[&str]) -> Self {
        self.classes = classes.into_iter().map(|s| s.to_string()).collect();
        self
    }

    pub fn dark(mut self, dark: bool) -> Self {
        self.dark = dark;
        self
    }

    pub(crate) fn class_string(&self) -> String {
        let mut classes = self.classes.clone();
        classes.push("toolkit-card".to_owned());

        if self.dark {
            classes.push("toolkit-dark".to_owned());
        }

        if self.full_width {
            classes.push("toolkit-card__full-width".to_owned());
        }

        classes.join(" ")
    }
}
