use crate::LinkKind;

#[derive(Debug, Default)]
pub struct Link {
    pub(crate) href: String,
    pub(crate) kind: LinkKind,
    pub(crate) title: Option<String>,
    classes: Vec<String>,
    plain: bool,
}

impl Link {
    pub fn current_tab(href: &str) -> Self {
        Link {
            href: href.to_owned(),
            ..Default::default()
        }
    }

    pub fn new_tab(href: &str) -> Self {
        Link {
            href: href.to_owned(),
            kind: LinkKind::NewTab,
            title: None,
            plain: false,
            classes: vec![],
        }
    }

    pub fn title(mut self, title: &str) -> Self {
        self.title = Some(title.to_owned());
        self
    }

    pub fn plain(mut self, plain: bool) -> Self {
        self.plain = plain;
        self
    }

    pub fn class(mut self, class: &str) -> Self {
        self.classes.push(class.to_owned());
        self
    }

    pub(crate) fn class_string(&self) -> String {
        let mut classes = self.classes.clone();

        classes.push("toolkit-link".to_owned());

        if self.plain {
            classes.push("toolkit-plain".to_owned());
        }

        classes.join(" ")
    }
}
