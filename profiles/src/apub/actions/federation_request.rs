use crate::{
    apub::actions::{
        AcceptFederationRequest, CreateFederationRequest, RejectFederationRequest,
        UndoFederationRequest,
    },
    Action, Context, Error, Outbound, Required,
};

impl Action for CreateFederationRequest {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        ctx.check_server_block(self.followed_by_server, self.followed_server)?;

        let follow_request = ctx.store.view.server_follow_requests.new(
            self.followed_server,
            self.followed_by_server,
            self.published,
        )?;

        if let Some(apub_id) = &self.follow_apub_id {
            ctx.apub.server_follow_request(apub_id, follow_request.id)?;
        }

        if ctx.is_self_server(self.followed_by_server)? {
            return Ok(Some(Box::new(crate::apub::results::Federation {
                follow_request_id: follow_request.id,
            })));
        }

        Ok(None)
    }
}

impl Action for AcceptFederationRequest {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let opt = ctx
            .store
            .view
            .server_follow_requests
            .remove(self.follow_request_id)?;

        if let Some(undo_follow_request) = opt {
            let follow = ctx.store.view.server_follows.new(
                undo_follow_request.0.left,
                undo_follow_request.0.right,
                self.published,
            )?;

            if let Some(apub_id) = &self.accept_apub_id {
                ctx.apub.server_follow(apub_id, follow.id)?;
            }

            if ctx.is_self_server(follow.left)? {
                let follow_apub_id = ctx
                    .apub
                    .apub_for_server_follow_request(undo_follow_request.0.id)?
                    .req("apub for follow request")?;
                return Ok(Some(Box::new(crate::apub::results::AcceptFederation {
                    follow_apub_id,
                    server_id: follow.left,
                    follow_id: follow.id,
                })));
            }
        }

        Ok(None)
    }
}

impl Action for RejectFederationRequest {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let opt = ctx
            .store
            .view
            .server_follow_requests
            .remove(self.follow_request_id)?;

        if let Some(undo_follow_request) = opt {
            if let Some(follow_apub_id) = ctx
                .apub
                .apub_for_server_follow_request(self.follow_request_id)?
            {
                ctx.apub.delete_object(&follow_apub_id)?;

                if ctx.is_self_server(undo_follow_request.0.left)? {
                    return Ok(Some(Box::new(crate::apub::results::RejectFederation {
                        follow_apub_id,
                        server_id: undo_follow_request.0.left,
                        requester_id: undo_follow_request.0.right,
                    })));
                }
            }
        }

        Ok(None)
    }
}

impl Action for UndoFederationRequest {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let opt = ctx
            .store
            .view
            .server_follow_requests
            .remove(self.follow_request_id)?;

        if let Some(undo_follow_request) = opt {
            if let Some(follow_apub_id) = ctx
                .apub
                .apub_for_server_follow_request(self.follow_request_id)?
            {
                ctx.apub.delete_object(&follow_apub_id)?;

                if ctx.is_self_server(undo_follow_request.0.right)? {
                    return Ok(Some(Box::new(crate::apub::results::UndoFederation {
                        follow_apub_id,
                        server_id: undo_follow_request.0.right,
                        followed_id: undo_follow_request.0.left,
                    })));
                }
            }
        }

        Ok(None)
    }
}
