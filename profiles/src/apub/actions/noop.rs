use crate::{apub::actions::Noop, Action, Context, Error, Outbound};

impl Action for Noop {
    fn perform(&self, _: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        Ok(None)
    }
}
