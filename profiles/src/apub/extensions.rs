use activitystreams::{
    actor::{ApActor, Application, Person},
    object::{ApObject, Note},
    unparsed::UnparsedMutExt,
    url::Url,
};
use activitystreams_ext::{Ext1, Ext2, Ext3, UnparsedExtension};

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Sensitive {
    #[serde(default)]
    pub(crate) sensitive: bool,
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ManuallyApprovesFollowers {
    #[serde(default)]
    pub(crate) manually_approves_followers: bool,
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
#[serde(rename_all = "camelCase")]
pub struct PropertyValue {
    #[serde(rename = "type")]
    pub(crate) kind: String,
    pub(crate) name: String,
    pub(crate) value: String,
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
#[serde(rename_all = "camelCase")]
pub struct PublicKey {
    pub(crate) public_key: PublicKeyInner,
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
#[serde(rename_all = "camelCase")]
pub struct PublicKeyInner {
    pub(crate) id: Url,
    pub(crate) owner: Url,
    pub(crate) public_key_pem: String,
}

impl<U> UnparsedExtension<U> for ManuallyApprovesFollowers
where
    U: UnparsedMutExt,
{
    type Error = serde_json::Error;

    fn try_from_unparsed(unparsed_mut: &mut U) -> Result<Self, Self::Error>
    where
        Self: Sized,
    {
        Ok(ManuallyApprovesFollowers {
            manually_approves_followers: unparsed_mut
                .remove("manuallyApprovesFollowers")
                .unwrap_or(false),
        })
    }

    fn try_into_unparsed(self, unparsed_mut: &mut U) -> Result<(), Self::Error> {
        unparsed_mut.insert(
            "manuallyApprovesFollowers",
            self.manually_approves_followers,
        )?;
        Ok(())
    }
}

impl<U> UnparsedExtension<U> for Sensitive
where
    U: UnparsedMutExt,
{
    type Error = serde_json::Error;

    fn try_from_unparsed(unparsed_mut: &mut U) -> Result<Self, Self::Error>
    where
        Self: Sized,
    {
        Ok(Sensitive {
            sensitive: unparsed_mut.remove("sensitive").unwrap_or(false),
        })
    }

    fn try_into_unparsed(self, unparsed_mut: &mut U) -> Result<(), Self::Error> {
        unparsed_mut.insert("sensitive", self.sensitive)?;
        Ok(())
    }
}

impl<U> UnparsedExtension<U> for PublicKey
where
    U: UnparsedMutExt,
{
    type Error = serde_json::Error;

    fn try_from_unparsed(unparsed_mut: &mut U) -> Result<Self, Self::Error> {
        Ok(PublicKey {
            public_key: unparsed_mut.remove("publicKey")?,
        })
    }

    fn try_into_unparsed(self, unparsed_mut: &mut U) -> Result<(), Self::Error> {
        unparsed_mut.insert("publicKey", self.public_key)?;
        Ok(())
    }
}

pub type ExtendedNote = Ext1<ApObject<Note>, Sensitive>;
pub type ExtendedPerson = Ext3<ApActor<Person>, PublicKey, ManuallyApprovesFollowers, Sensitive>;
pub type ExtendedApplication = Ext2<ApActor<Application>, PublicKey, ManuallyApprovesFollowers>;
