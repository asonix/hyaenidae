(function(fn) {
    if (document.readyState === "complete" || document.readyState === "interactive") {
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
})(function() {
    function selectedMessage(multiple, files) {
        if (multiple && files.length == 1) {
            return "1 Selected File";
        } else if (multiple) {
            return "0 Selected files";
        } else {
            return "Selected: " + files[0].name;
        }
    }

    function onSelect(input, span) {
        return function(event) {
            if (event.target.files) {
                span.textContent = selectedMessage(input.multiple, event.target.files);
            }
        };
    }

    var file_inputs = document.getElementsByClassName("toolkit-file");

    for (var i = 0; i < file_inputs.length; i++) {
        var span = file_inputs[i].getElementsByTagName("span")[0];
        var input = file_inputs[i].getElementsByTagName("input")[0];

        if (!span || !input) {
            console.error("Error fetching file upload for", file_inputs[i]);
            continue;
        }

        input.addEventListener("change", onSelect(input, span));
    }
})
