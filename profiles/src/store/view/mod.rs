use sled::Db;

mod notif;
mod relationship;

use notif::NotificationStore;
use relationship::RelationshipStore;

pub use relationship::Relationship;

#[derive(Clone, Debug)]
pub struct Store {
    pub submissions: NotificationStore,
    pub comments: NotificationStore,
    pub reacts: NotificationStore,
    pub follow_request_notifs: NotificationStore,

    pub server_blocks: RelationshipStore,
    pub server_follows: RelationshipStore,
    pub server_follow_requests: RelationshipStore,

    pub blocks: RelationshipStore,
    pub follows: RelationshipStore,
    pub follow_requests: RelationshipStore,
}

impl Store {
    pub(super) fn build(db: &Db) -> Result<Self, sled::Error> {
        Ok(Store {
            // notifications
            submissions: NotificationStore::build("submissions", db)?,
            comments: NotificationStore::build("comments", db)?,
            reacts: NotificationStore::build("reacts", db)?,
            follow_request_notifs: NotificationStore::build("follow-requests", db)?,

            // relationships

            // left blocked by right
            // right blocked left
            server_blocks: RelationshipStore::build("server-blocks", db)?,

            // left followed by right
            // right followed left
            server_follows: RelationshipStore::build("server-follows", db)?,

            // left followed by right
            // right requested to follow left
            server_follow_requests: RelationshipStore::build("server-follow-requests", db)?,

            // left blocked by right
            // right blocking left
            blocks: RelationshipStore::build("blocks", db)?,

            // left follwed by right
            // right following left
            follows: RelationshipStore::build("follows", db)?,

            // left followed by right
            // right requested to follow left
            follow_requests: RelationshipStore::build("follow-requests", db)?,
        })
    }
}
