#!/usr/bin/env bash

set -xe

pushd ../../server
cargo build
popd

cp ../../target/debug/hyaenidae-server .

if mkdir volumes; then
    mkdir -p volumes/pictrs-{1,2,3}
    sudo chown -R 991:991 volumes/pictrs-{1,2,3}
fi

docker-compose build
docker-compose up
