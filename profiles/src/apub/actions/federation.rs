use crate::{
    apub::actions::{UndoAcceptFederation, UndoFederation},
    Action, Context, Error, Outbound, Required,
};
use url::Url;

// Get Federation from Undo Federation or Accept Federation or Reject Federation
fn follow_apub_id(parent_apub_id: &Url, ctx: &Context) -> Result<Url, Error> {
    use activitystreams::{activity::ActorAndObject, prelude::*};
    let parent_any_base = ctx
        .apub
        .object(parent_apub_id)?
        .req("apub for parent object")?;
    let parent: ActorAndObject<String> = parent_any_base
        .extend()?
        .req("actor-and-object for parent")?;
    let follow_id = parent
        .object()
        .as_single_id()
        .req("child object id from parent")?;
    Ok(follow_id.to_owned())
}

impl Action for UndoFederation {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        log::debug!("UNDO FEDERATION");
        let opt = ctx.store.view.server_follows.remove(self.follow_id)?;

        if let Some(accept_apub_id) = ctx.apub.apub_for_server_follow(self.follow_id)? {
            log::debug!("Getting follow apub id");
            let follow_apub_id = follow_apub_id(&accept_apub_id, ctx)?;
            log::debug!("Got it");
            ctx.apub.delete_object(&accept_apub_id)?;
            ctx.apub.delete_object(&follow_apub_id)?;
            log::debug!("Deleted objects");

            if let Some(undo_follow) = opt {
                log::debug!("undo opt");
                if ctx.is_self_server(undo_follow.0.right)? {
                    log::debug!("Outbound UndoFederation");
                    return Ok(Some(Box::new(crate::apub::results::UndoFederation {
                        follow_apub_id,
                        server_id: undo_follow.0.right,
                        followed_id: undo_follow.0.left,
                    })));
                }
            }
        }

        Ok(None)
    }
}

impl Action for UndoAcceptFederation {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        log::debug!("UNDO ACCEPT");
        let opt = ctx.store.view.server_follows.remove(self.follow_id)?;

        if let Some(accept_apub_id) = ctx.apub.apub_for_server_follow(self.follow_id)? {
            log::debug!("Getting follow apub id");
            let follow_apub_id = follow_apub_id(&accept_apub_id, ctx)?;
            log::debug!("Got it");
            ctx.apub.delete_object(&accept_apub_id)?;
            ctx.apub.delete_object(&follow_apub_id)?;
            log::debug!("Deleted objects");

            if let Some(undo_follow) = opt {
                log::debug!("undo opt");
                if ctx.is_self_server(undo_follow.0.left)? {
                    log::debug!("Outbound UndoAcceptFederation");
                    return Ok(Some(Box::new(crate::apub::results::UndoAcceptFederation {
                        accept_apub_id,
                        server_id: undo_follow.0.left,
                        requester_id: undo_follow.0.right,
                    })));
                }
            }
        }

        Ok(None)
    }
}
