use crate::{
    apub::actions::{UndoAcceptFollow, UndoFollow},
    Action, Context, Error, Outbound, Required,
};
use url::Url;

// Get Follow from Undo Follow or Accept Follow or Reject Follow
fn follow_apub_id(parent_apub_id: &Url, ctx: &Context) -> Result<Url, Error> {
    use activitystreams::{activity::ActorAndObject, prelude::*};
    let parent_any_base = ctx
        .apub
        .object(parent_apub_id)?
        .req("apub for parent object")?;
    let parent: ActorAndObject<String> = parent_any_base
        .extend()?
        .req("actor-and-object for parent")?;
    let follow_id = parent
        .object()
        .as_single_id()
        .req("child object from parent")?;
    Ok(follow_id.to_owned())
}

impl Action for UndoFollow {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let opt = ctx.store.view.follows.remove(self.follow_id)?;

        if let Some(accept_apub_id) = ctx.apub.apub_for_follow(self.follow_id)? {
            let follow_apub_id = follow_apub_id(&accept_apub_id, ctx)?;
            ctx.apub.delete_object(&accept_apub_id)?;
            ctx.apub.delete_object(&follow_apub_id)?;

            if let Some(undo_follow) = opt {
                if ctx.is_local(undo_follow.0.right)? || !ctx.is_local(undo_follow.0.left)? {
                    return Ok(Some(Box::new(crate::apub::results::UndoFollow {
                        follow_apub_id,
                        profile_id: undo_follow.0.right,
                        followed_id: undo_follow.0.left,
                    })));
                }
            }
        }

        Ok(None)
    }
}

impl Action for UndoAcceptFollow {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let opt = ctx.store.view.follows.remove(self.follow_id)?;

        if let Some(accept_apub_id) = ctx.apub.apub_for_follow(self.follow_id)? {
            let follow_apub_id = follow_apub_id(&accept_apub_id, ctx)?;
            ctx.apub.delete_object(&accept_apub_id)?;
            ctx.apub.delete_object(&follow_apub_id)?;

            if let Some(undo_follow) = opt {
                if ctx.is_local(undo_follow.0.left)? && !ctx.is_local(undo_follow.0.right)? {
                    return Ok(Some(Box::new(crate::apub::results::UndoAcceptFollow {
                        accept_apub_id,
                        profile_id: undo_follow.0.left,
                        requester_id: undo_follow.0.right,
                    })));
                }
            }
        }

        Ok(None)
    }
}
