use crate::{
    apub::{
        actions::{CreateReport, ForwardReport, Reported, Reporter, ResolveReport},
        results::ReportCreated,
    },
    store::{ReportKind, ReporterKind},
    Action, Context, Error, Outbound,
};
use uuid::Uuid;

impl Reported {
    fn id(&self) -> Uuid {
        match self {
            Reported::Submission(id) => *id,
            Reported::Comment(id) => *id,
            Reported::Profile(id) => *id,
        }
    }

    fn kind(&self) -> ReportKind {
        match self {
            Reported::Submission(_) => ReportKind::Submission,
            Reported::Comment(_) => ReportKind::Comment,
            Reported::Profile(_) => ReportKind::Profile,
        }
    }
}

impl Reporter {
    fn id(&self) -> Uuid {
        match self {
            Reporter::Profile(id) => *id,
            Reporter::Server(id) => *id,
        }
    }

    fn kind(&self) -> ReporterKind {
        match self {
            Reporter::Profile(_) => ReporterKind::Profile,
            Reporter::Server(_) => ReporterKind::Server,
        }
    }
}

impl Action for CreateReport {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let report = ctx.store.reports.report(
            self.reported_item.id(),
            self.reported_item.kind(),
            self.reporter.id(),
            self.reporter.kind(),
            self.note.as_deref().map(hyaenidae_content::html),
        )?;

        if let Some(apub_id) = &self.flag_apub_id {
            ctx.apub.report(&apub_id, report.id())?;
        }

        Ok(None)
    }
}

impl Action for ForwardReport {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        if ctx.store.reports.forwarded(self.report_id)?.is_none() {
            ctx.store.reports.forward_report(self.report_id)?;
            Ok(Some(Box::new(ReportCreated {
                report_id: self.report_id,
            })))
        } else {
            Ok(None)
        }
    }
}

impl Action for ResolveReport {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        ctx.store
            .reports
            .resolve_report(self.report_id, self.resolution.clone())?;
        Ok(None)
    }
}
