use crate::{
    apub::actions::{apub::require_federation, CreateReport, Reported, Reporter},
    recover, Action, Context, Error, KeyOwner, RecoverableError, Required,
};
use activitystreams::prelude::*;

pub(crate) fn flag(
    flag: &activitystreams::activity::Flag,
    key_owner: Option<KeyOwner>,
    ctx: &Context,
) -> Result<Result<Box<dyn Action>, RecoverableError>, Error> {
    let flag_id = flag.id_unchecked().req("flag id")?;

    // Double create
    if ctx.apub.object(flag_id)?.is_some() {
        return Err(Error::Invalid);
    }

    let actor = flag.actor()?.as_single_id().req("flag actor id")?;
    require_federation(key_owner, actor, ctx)?;

    let object = flag.object().as_single_id().req("flag object id")?;
    let content = flag.content().req("content")?;
    let report_note = content.as_single_xsd_string();

    let actor_id = recover!(actor, ctx.apub.id_for_apub(actor)?);
    let actor_id = actor_id.server().req("flag actor id as server id")?;

    let object_id = recover!(object, ctx.apub.id_for_apub(object)?);

    if let Some(submission_id) = object_id.submission() {
        return Ok(Ok(Box::new(CreateReport {
            flag_apub_id: Some(flag_id.to_owned()),
            reported_item: Reported::Submission(submission_id),
            reporter: Reporter::Server(actor_id),
            note: report_note.map(|s| s.to_string()),
        })));
    }

    if let Some(comment_id) = object_id.comment() {
        return Ok(Ok(Box::new(CreateReport {
            flag_apub_id: Some(flag_id.to_owned()),
            reported_item: Reported::Comment(comment_id),
            reporter: Reporter::Server(actor_id),
            note: report_note.map(|s| s.to_string()),
        })));
    }

    if let Some(profile_id) = object_id.profile() {
        return Ok(Ok(Box::new(CreateReport {
            flag_apub_id: Some(flag_id.to_owned()),
            reported_item: Reported::Profile(profile_id),
            reporter: Reporter::Server(actor_id),
            note: report_note.map(|s| s.to_string()),
        })));
    }

    Err(Error::Invalid)
}
