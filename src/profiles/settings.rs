use crate::error::Error;
use sled::{Db, Tree};
use uuid::Uuid;

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
pub(crate) struct Settings {
    pub(crate) dark: bool,
    pub(crate) sensitive: bool,
}

#[derive(Clone, Debug)]
pub(crate) struct SettingStore {
    tree: Tree,
}

impl SettingStore {
    pub(crate) fn build(db: &Db) -> Result<Self, sled::Error> {
        Ok(SettingStore {
            tree: db.open_tree("/main/settings")?,
        })
    }

    pub(crate) async fn for_profile(&self, profile_id: Uuid) -> Result<Settings, Error> {
        let this = self.clone();

        let opt = actix_web::web::block(move || {
            Ok(this
                .tree
                .get(profile_id.as_bytes())?
                .and_then(|ivec| serde_json::from_slice(&ivec).ok()))
                as Result<Option<_>, Error>
        })
        .await??;

        Ok(opt.unwrap_or(Settings {
            dark: true,
            sensitive: false,
        }))
    }

    pub(crate) async fn update(&self, profile_id: Uuid, settings: Settings) -> Result<(), Error> {
        let this = self.clone();

        actix_web::web::block(move || {
            let vec = serde_json::to_vec(&settings)?;
            this.tree.insert(profile_id.as_bytes(), vec)?;
            Ok(()) as Result<_, Error>
        })
        .await??;

        Ok(())
    }
}
