use crate::{
    apub::{
        actions::{apub::require_federation, CreateBlock, DeleteBlock},
        ExtendedPerson,
    },
    recover, Action, Context, Error, KeyOwner, RecoverableError, Required,
};
use activitystreams::prelude::*;

pub(crate) fn block_person(
    block: &activitystreams::activity::Block,
    person: &ExtendedPerson,
    key_owner: Option<KeyOwner>,
    ctx: &Context,
) -> Result<Result<Box<dyn Action>, RecoverableError>, Error> {
    let id = block.id_unchecked().req("block id")?;

    // Double create
    if ctx.apub.object(id)?.is_some() {
        return Err(Error::Invalid);
    }

    let actor_id = block.actor()?.as_single_id().req("block actor id")?;
    require_federation(key_owner, actor_id, ctx)?;

    let published = block.published().req("published")?;

    let actor = recover!(actor_id, ctx.apub.id_for_apub(actor_id)?);
    let actor = actor.profile().req("block actor id as profile id")?;

    if let Some(actor_profile) = ctx.store.profiles.by_id(actor)? {
        if actor_profile.is_suspended() {
            return Err(Error::Invalid);
        }
    } else {
        return Err(Error::Invalid);
    }

    let object_id = person.id_unchecked().req("person id")?;
    let object = recover!(object_id, ctx.apub.id_for_apub(object_id)?);
    let object = object.profile().req("person id as profile id")?;

    if !ctx.store.profiles.by_id(object)?.is_some() {
        return Err(Error::Invalid);
    }

    if let Some(true) = ctx.store.profiles.is_local(object)? {
        return Ok(Ok(Box::new(CreateBlock {
            block_apub_id: Some(id.to_owned()),
            blocked_profile: object,
            blocked_by_profile: actor,
            published: published.into(),
        })));
    }

    Err(Error::Invalid)
}

pub(crate) fn undo_block_person(
    undo: &activitystreams::activity::Undo,
    block: &activitystreams::activity::Block,
    _person: &ExtendedPerson,
    key_owner: Option<KeyOwner>,
    ctx: &Context,
) -> Result<Result<Box<dyn Action>, RecoverableError>, Error> {
    let undo_actor = undo.actor()?.as_single_id().req("undo actor id")?;
    require_federation(key_owner, undo_actor, ctx)?;

    let block_actor = block.actor()?.as_single_id().req("block actor id")?;

    if block_actor != undo_actor {
        return Err(Error::Invalid);
    }

    let actor = recover!(undo_actor, ctx.apub.id_for_apub(undo_actor)?);
    let actor = actor.profile().req("undo actor id as profile id")?;

    if let Some(actor_profile) = ctx.store.profiles.by_id(actor)? {
        if actor_profile.is_suspended() {
            return Err(Error::Invalid);
        }
    } else {
        return Err(Error::Invalid);
    }

    let block_id = block.id_unchecked().req("block id")?;
    let id = recover!(block_id, ctx.apub.id_for_apub(block_id)?);
    let id = id.block().req("block id as block id")?;

    Ok(Ok(Box::new(DeleteBlock { block_id: id })))
}
