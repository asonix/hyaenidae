use crate::image::Image;
use crate::tile::{Indicator, IndicatorColor};
use std::rc::Rc;

pub(crate) struct ThumbnailPart {
    pub(crate) text: String,
    pub(crate) href: String,
}

pub struct Thumbnail {
    image: Rc<dyn Image>,
    pub(crate) href: String,
    pub(crate) title: Option<String>,
    pub(crate) author: Option<ThumbnailPart>,
    pub(crate) indicator: Option<Indicator>,
}

impl Thumbnail {
    pub fn new(image: impl Image + 'static, href: &str) -> Self {
        Thumbnail {
            image: Rc::new(image),
            href: href.to_owned(),
            title: None,
            author: None,
            indicator: None,
        }
    }

    pub fn indicator(mut self, text: &str, color: IndicatorColor) -> Self {
        self.indicator = Some(Indicator {
            text: text.to_owned(),
            color,
        });
        self
    }

    pub fn title(mut self, text: &str) -> Self {
        self.title = Some(text.to_owned());
        self
    }

    pub fn author(mut self, text: &str, href: &str) -> Self {
        self.author = Some(ThumbnailPart {
            text: text.to_owned(),
            href: href.to_owned(),
        });
        self
    }

    pub(crate) fn image(&self) -> &dyn Image {
        &*self.image
    }
}
