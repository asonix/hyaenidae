use crate::{to_home, to_register, AcceptedCookies, Error, State};
use actix_session::Session;
use actix_web::{web, HttpResponse};

pub type CookiesPageArgs = (Option<AcceptedCookies>, web::Data<State>);

pub type CookiesArgs = (Session, web::Data<State>);

#[derive(Debug)]
pub struct CookiesState {
    state: State,
}

pub fn cookies_page((cookies, state): CookiesPageArgs) -> Result<CookiesState, HttpResponse> {
    if cookies.is_some() {
        return Err(to_home(&state));
    }

    Ok(CookiesState::new(&state))
}

pub async fn cookies((session, state): CookiesArgs) -> Result<HttpResponse, Error> {
    crate::extractors::CookieData::set_accepted(&session)?;

    Ok(to_register(&state))
}

impl CookiesState {
    fn new(state: &State) -> Self {
        CookiesState {
            state: state.clone(),
        }
    }

    pub fn cookies_path(&self) -> String {
        self.state.pages.cookies_path()
    }

    pub fn home_path(&self) -> String {
        self.state.pages.home_path()
    }
}
