use super::{AcceptFollow, Follow as FollowRequested, RejectFollow, UndoAcceptFollow, UndoFollow};
use crate::{Context, Error, OnBehalfOf, Outbound, Required};
use activitystreams::{
    activity::{Accept, Follow, Reject, Undo},
    base::AnyBase,
    context,
    prelude::*,
    security,
};
use url::Url;
use uuid::Uuid;

impl Outbound for FollowRequested {
    fn id(&self) -> Option<Uuid> {
        Some(self.follow_request_id)
    }

    fn behalf(&self, ctx: &Context) -> Result<OnBehalfOf, Error> {
        let follower_id = ctx
            .store
            .view
            .follow_requests
            .right(self.follow_request_id)?
            .req("follower profile id by follow id")?;

        Ok(OnBehalfOf::Profile(follower_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let followed_id = ctx
            .store
            .view
            .follow_requests
            .left(self.follow_request_id)?
            .req("followed profile id by follow id")?;
        let inbox = ctx
            .apub
            .endpoints_for_profile(followed_id)?
            .req("endpoints for followed profile")?
            .inbox()
            .to_owned();

        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let follow = ctx
            .store
            .view
            .follow_requests
            .by_id(self.follow_request_id)?
            .req("follow request by id")?;
        let actor_id = ctx
            .apub
            .apub_for_profile(follow.right)?
            .req("apub for following profile")?;
        let object_id = ctx
            .apub
            .apub_for_profile(follow.left)?
            .req("apub for followed profile")?;
        let published = follow.published;

        let follow_id = ctx.apub.info.gen_id().req("id generation")?;
        ctx.apub
            .follow_request(&follow_id, self.follow_request_id)?;

        let mut follow = Follow::new(actor_id, object_id);
        follow
            .set_id(follow_id)
            .set_published(published.into())
            .add_context(context())
            .add_context(security());
        let follow = follow.into_any_base()?;
        ctx.apub.store_object(&follow)?;

        Ok(follow)
    }
}

impl Outbound for RejectFollow {
    fn id(&self) -> Option<Uuid> {
        None
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Profile(self.profile_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let inbox = ctx
            .apub
            .endpoints_for_profile(self.requester_id)?
            .req("endpoints for requesting profile")?
            .inbox()
            .to_owned();

        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let person_id = ctx
            .apub
            .apub_for_profile(self.profile_id)?
            .req("apub for profile")?;
        let follow_request = ctx
            .apub
            .object(&self.follow_apub_id)?
            .req("apub for follow")?;

        let mut reject = Reject::new(person_id, follow_request);
        reject
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security());
        let reject = reject.into_any_base()?;
        ctx.apub.store_object(&reject)?;

        Ok(reject)
    }
}

impl Outbound for AcceptFollow {
    fn id(&self) -> Option<Uuid> {
        Some(self.follow_id)
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Profile(self.profile_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let requester_id = ctx
            .store
            .view
            .follows
            .right(self.follow_id)?
            .req("follower by follow id")?;

        let inbox = ctx
            .apub
            .endpoints_for_profile(requester_id)?
            .req("endpoints for requester")?
            .inbox()
            .to_owned();

        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let person_id = ctx
            .apub
            .apub_for_profile(self.profile_id)?
            .req("apub for profile")?;
        let follow_request = ctx
            .apub
            .object(&self.follow_apub_id)?
            .req("apub for follow")?;

        let accept_id = ctx.apub.info.gen_id().req("id generation")?;
        ctx.apub.follow(&accept_id, self.follow_id)?;

        let mut accept = Accept::new(person_id, follow_request);
        accept
            .set_id(accept_id)
            .add_context(context())
            .add_context(security());
        let accept = accept.into_any_base()?;
        ctx.apub.store_object(&accept)?;

        Ok(accept)
    }
}

impl Outbound for UndoAcceptFollow {
    fn id(&self) -> Option<Uuid> {
        None
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Profile(self.profile_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let inbox = ctx
            .apub
            .endpoints_for_profile(self.requester_id)?
            .req("endpoints for requesting profile")?
            .inbox()
            .to_owned();

        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let person_id = ctx
            .apub
            .apub_for_profile(self.profile_id)?
            .req("apub for profile")?;
        let accept = ctx
            .apub
            .object(&self.accept_apub_id)?
            .req("apub for accept")?;

        let mut undo = Undo::new(person_id, accept);
        undo.set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security());
        let undo = undo.into_any_base()?;
        ctx.apub.store_object(&undo)?;

        Ok(undo)
    }
}

impl Outbound for UndoFollow {
    fn id(&self) -> Option<Uuid> {
        None
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Profile(self.profile_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let inbox = ctx
            .apub
            .endpoints_for_profile(self.followed_id)?
            .req("endpoints for followed profile")?
            .inbox()
            .to_owned();

        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let person_id = ctx
            .apub
            .apub_for_profile(self.profile_id)?
            .req("apub for profile")?;
        let follow = ctx
            .apub
            .object(&self.follow_apub_id)?
            .req("apub for follow")?;

        let mut undo = Undo::new(person_id, follow);
        undo.set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security());
        let undo = undo.into_any_base()?;
        ctx.apub.store_object(&undo)?;

        Ok(undo)
    }
}
