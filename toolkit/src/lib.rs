include!(concat!(env!("OUT_DIR"), "/templates.rs"));

mod banner;
mod button;
mod card;
mod file_input;
mod icon;
mod image;
mod input;
mod link;
mod profile;
mod select;
mod tab;
mod text_input;
mod thumbnail;
mod tile;

pub use self::{
    banner::Banner,
    button::{Button, ButtonKind, LinkKind},
    card::Card,
    file_input::FileInput,
    icon::{Icon, Size},
    image::Image,
    input::{Input, InputKind},
    link::Link,
    profile::Profile,
    select::Select,
    tab::Tab,
    text_input::TextInput,
    thumbnail::Thumbnail,
    tile::{IndicatorColor, Tile, Tiles},
};
