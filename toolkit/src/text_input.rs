use crate::{Input, InputKind};

#[derive(Debug, Default)]
pub struct TextInput {
    pub(crate) input: Input,
    pub(crate) title: Option<String>,
    pub(crate) classes: Vec<String>,
    pub(crate) error: Option<String>,
    dark: bool,
}

impl TextInput {
    pub fn new(name: &str) -> Self {
        TextInput {
            input: Input::new(name, InputKind::Text),
            ..Default::default()
        }
    }

    pub fn password(self) -> Self {
        TextInput {
            input: self.input.kind(InputKind::Password),
            ..self
        }
    }

    pub fn textarea(self) -> Self {
        TextInput {
            input: self.input.kind(InputKind::TextArea),
            ..self
        }
    }

    pub fn title(mut self, title: &str) -> Self {
        self.title = Some(title.to_owned());
        self
    }

    pub fn value(self, value: &str) -> Self {
        TextInput {
            input: self.input.value(value),
            ..self
        }
    }

    pub fn placeholder(self, placeholder: &str) -> Self {
        TextInput {
            input: self.input.placeholder(placeholder),
            ..self
        }
    }

    pub fn classes(mut self, classes: &[&str]) -> Self {
        self.classes = classes.into_iter().map(|s| s.to_string()).collect();
        self
    }

    pub fn value_opt(self, value: Option<String>) -> Self {
        TextInput {
            input: self.input.value_opt(value),
            ..self
        }
    }

    pub fn error_opt(mut self, error: Option<String>) -> Self {
        self.error = error;
        self
    }

    pub fn dark(mut self, dark: bool) -> Self {
        self.dark = dark;
        self
    }

    pub(crate) fn class_string(&self) -> String {
        let mut classes = self.classes.clone();

        classes.push("toolkit-input".to_owned());

        if self.dark {
            classes.push("toolkit-dark".to_owned());
        }

        classes.join(" ")
    }
}
