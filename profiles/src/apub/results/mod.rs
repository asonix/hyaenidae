use url::Url;
use uuid::Uuid;

mod block;
mod comment;
mod federation_request;
mod follow_request;
mod profile;
mod react;
mod report;
mod server;
mod submission;

pub(super) struct ReportCreated {
    pub(super) report_id: Uuid,
}

pub(super) struct ServerCreated {
    pub(super) server_id: Uuid,
}

pub(super) struct ServerUpdated {
    pub(super) server_id: Uuid,
}

pub(super) struct ProfileCreated {
    pub(super) profile_id: Uuid,
}

pub(super) struct ProfileUpdated {
    pub(super) profile_id: Uuid,
}

pub(super) struct ProfileDeleted {
    pub(super) profile_id: Uuid,
}

pub(super) struct SubmissionCreated {
    pub(super) submission_id: Uuid,
}

pub(super) struct LocalSubmissionCreated {
    pub(super) submission_id: Uuid,
}

pub(super) struct UnpublishedSubmissionCreated {
    pub(super) submission_id: Uuid,
}

pub(super) struct SubmissionUpdated {
    pub(super) submission_id: Uuid,
}

pub(super) struct LocalSubmissionUpdated {
    pub(super) submission_id: Uuid,
}

pub(super) struct SubmissionDeleted {
    pub(super) profile_id: Uuid,
    pub(super) submission_id: Uuid,
}

pub(super) struct CommentCreated {
    pub(super) comment_id: Uuid,
}

pub(super) struct LocalCommentCreated {
    pub(super) comment_id: Uuid,
}

pub(super) struct AnnounceCommentCreated {
    pub(super) submissioner_id: Uuid,
    pub(super) comment_id: Uuid,
}

pub(super) struct RemoteCommentCreated {
    pub(super) note_apub_id: Url,
    pub(super) submissioner_id: Uuid,
}

pub(super) struct CommentUpdated {
    pub(super) comment_id: Uuid,
}

pub(super) struct LocalCommentUpdated {
    pub(super) comment_id: Uuid,
}

pub(super) struct AnnounceCommentUpdated {
    pub(super) submissioner_id: Uuid,
    pub(super) comment_id: Uuid,
}

pub(super) struct RemoteCommentUpdated {
    pub(super) update_apub_id: Url,
    pub(super) submissioner_id: Uuid,
}

pub(super) struct CommentDeleted {
    pub(super) note_apub_id: Url,
    pub(super) commenter_id: Uuid,
    pub(super) submission_id: Uuid,
    pub(super) reply_to_id: Option<Uuid>,
}

pub(super) struct AnnounceCommentDeleted {
    pub(super) note_apub_id: Url,
    pub(super) submissioner_id: Uuid,
    pub(super) commenter_id: Uuid,
}

pub(super) struct RemoteCommentDeleted {
    pub(super) delete_apub_id: Url,
    pub(super) submissioner_id: Uuid,
}

pub(super) struct React {
    pub(super) react_id: Uuid,
}

pub(super) struct UndoReact {
    pub(super) like_apub_id: Url,
    pub(super) profile_id: Uuid,
    pub(super) submission_id: Uuid,
    pub(super) comment_id: Option<Uuid>,
}

pub(super) struct Follow {
    pub(super) follow_request_id: Uuid,
}

pub(super) struct RejectFollow {
    pub(super) follow_apub_id: Url,
    pub(super) profile_id: Uuid,
    pub(super) requester_id: Uuid,
}

pub(super) struct AcceptFollow {
    pub(super) follow_apub_id: Url,
    pub(super) profile_id: Uuid,
    pub(super) follow_id: Uuid,
}

pub(super) struct UndoAcceptFollow {
    pub(super) accept_apub_id: Url,
    pub(super) profile_id: Uuid,
    pub(super) requester_id: Uuid,
}

pub(super) struct UndoFollow {
    pub(super) follow_apub_id: Url,
    pub(super) profile_id: Uuid,
    pub(super) followed_id: Uuid,
}

pub(super) struct Federation {
    pub(super) follow_request_id: Uuid,
}

pub(super) struct RejectFederation {
    pub(super) follow_apub_id: Url,
    pub(super) server_id: Uuid,
    pub(super) requester_id: Uuid,
}

pub(super) struct AcceptFederation {
    pub(super) follow_apub_id: Url,
    pub(super) server_id: Uuid,
    pub(super) follow_id: Uuid,
}

pub(super) struct UndoAcceptFederation {
    pub(super) accept_apub_id: Url,
    pub(super) server_id: Uuid,
    pub(super) requester_id: Uuid,
}

pub(super) struct UndoFederation {
    pub(super) follow_apub_id: Url,
    pub(super) server_id: Uuid,
    pub(super) followed_id: Uuid,
}

pub(super) struct Block {
    pub(super) block_id: Uuid,
}

pub(super) struct UndoBlock {
    pub(super) block_apub_id: Url,
    pub(super) profile_id: Uuid,
    pub(super) blocked_id: Uuid,
}
