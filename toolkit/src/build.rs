use ructe::Ructe;

fn main() -> ructe::Result<()> {
    let mut ructe = Ructe::from_env()?;
    let mut statics = ructe.statics()?;
    statics.add_files("static/css")?;
    statics.add_files("static/js")?;
    statics.add_files_as("static/fonts", "fonts")?;
    statics.add_sass_file("scss/toolkit.scss")?;
    ructe.compile_templates("templates")?;

    Ok(())
}
