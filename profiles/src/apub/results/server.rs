use super::{ServerCreated, ServerUpdated};
use crate::{
    apub::{ExtendedApplication, ManuallyApprovesFollowers, PublicKey, PublicKeyInner},
    Context, Error, OnBehalfOf, Outbound, Required,
};
use activitystreams::{
    activity::{Create, Update},
    actor::{ApActor, Application, Endpoints},
    base::AnyBase,
    context,
    prelude::*,
    security,
};
use std::collections::HashSet;
use url::Url;
use uuid::Uuid;

fn application_context() -> AnyBase {
    AnyBase::from_arbitrary_json(serde_json::json!({
        "manuallyApprovesFollowers": "as:manuallyApprovesFollowers",
    }))
    .unwrap()
}

impl Outbound for ServerCreated {
    fn id(&self) -> Option<Uuid> {
        Some(self.server_id)
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Server)
    }

    fn inboxes(&self, _: &Context) -> Result<Vec<Url>, Error> {
        Ok(vec![])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let server = ctx
            .store
            .servers
            .by_id(self.server_id)?
            .req("server by id")?;

        let application_id = ctx.apub.info.gen_id().req("id generation")?;
        let public_key_id = ctx
            .apub
            .info
            .public_key(&application_id)
            .req("public key id")?;
        let following = ctx.apub.info.following(&application_id).req("following")?;
        let followers = ctx.apub.info.followers(&application_id).req("followers")?;
        let inbox = ctx.apub.info.inbox(&application_id).req("inbox")?;
        let outbox = ctx.apub.info.outbox(&application_id).req("outbox")?;
        let shared_inbox = ctx.apub.info.shared_inbox();

        ctx.apub.server(&application_id, server.id())?;
        ctx.apub.store_server_endpoints(
            server.id(),
            &application_id,
            crate::apub::Endpoints {
                inbox: inbox.clone(),
                outbox: Some(outbox.clone()),
                following: Some(following.clone()),
                followers: Some(followers.clone()),
                shared_inbox: Some(shared_inbox.clone()),
                public_key: public_key_id.clone(),
            },
        )?;
        let public_key_pem = ctx.apub.gen_server_keys(server.id(), &public_key_id)?;

        let mut application = ExtendedApplication::new(
            ApActor::new(inbox, Application::new()),
            PublicKey {
                public_key: PublicKeyInner {
                    id: public_key_id,
                    owner: application_id.clone(),
                    public_key_pem,
                },
            },
            ManuallyApprovesFollowers {
                manually_approves_followers: true,
            },
        );

        application
            .set_id(application_id.clone())
            .set_following(following)
            .set_followers(followers)
            .set_outbox(outbox)
            .set_endpoints(Endpoints {
                shared_inbox: Some(shared_inbox),
                ..Endpoints::default()
            })
            .set_preferred_username(server.domain())
            .set_published(server.created().into());
        if let Some(updated) = server.updated() {
            application.set_updated(updated.into());
        }

        if let Some(title) = server.title() {
            application.set_name(title);
        }
        if let Some(description) = server.description() {
            application.set_summary(description);
        }

        let any_base = application.into_any_base()?;
        ctx.apub.store_object(&any_base)?;

        let mut create = Create::new(application_id.clone(), any_base);
        create
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(application_context());

        let any_base = create.into_any_base()?;
        ctx.apub.store_object(&any_base)?;

        log::info!("Created activitypub server with ID {}", application_id);

        Ok(any_base)
    }
}

impl Outbound for ServerUpdated {
    fn id(&self) -> Option<Uuid> {
        Some(self.server_id)
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Server)
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let follower_inboxes = ctx
            .store
            .federated_servers()
            .filter_map(|server_id| {
                Some(
                    ctx.apub
                        .endpoints_for_server(server_id)
                        .ok()??
                        .inbox()
                        .to_owned(),
                )
            })
            .collect::<HashSet<_>>();

        Ok(follower_inboxes.into_iter().collect())
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let server = ctx
            .store
            .servers
            .by_id(self.server_id)?
            .req("server by id")?;
        let endpoints = ctx
            .apub
            .endpoints_for_server(self.server_id)?
            .req("endpoints for server")?;
        let public_key_id = ctx
            .apub
            .key_for_server(server.id())?
            .req("key for server")?;
        let public_key_pem = ctx
            .apub
            .public_key_for_id(&public_key_id)?
            .req("public key for id")?;
        let application_id = ctx
            .apub
            .apub_for_server(self.server_id)?
            .req("apub id for server")?;

        let mut application = ExtendedApplication::new(
            ApActor::new(endpoints.inbox, Application::new()),
            PublicKey {
                public_key: PublicKeyInner {
                    id: public_key_id,
                    owner: application_id.clone(),
                    public_key_pem,
                },
            },
            ManuallyApprovesFollowers {
                manually_approves_followers: true,
            },
        );

        application
            .set_id(application_id.clone())
            .set_preferred_username(server.domain())
            .set_published(server.created().into());

        if let Some(updated) = server.updated() {
            application.set_updated(updated.into());
        }
        if let Some(outbox) = endpoints.outbox {
            application.set_outbox(outbox);
        }
        if let Some(following) = endpoints.following {
            application.set_following(following);
        }
        if let Some(followers) = endpoints.followers {
            application.set_followers(followers);
        }
        if let Some(shared_inbox) = endpoints.shared_inbox {
            application.set_endpoints(Endpoints {
                shared_inbox: Some(shared_inbox),
                ..Endpoints::default()
            });
        }

        if let Some(title) = server.title() {
            application.set_name(title);
        }
        if let Some(description) = server.description() {
            application.set_summary(description);
        }

        let any_base = application.into_any_base()?;
        ctx.apub.store_object(&any_base)?;

        let mut update = Update::new(application_id, any_base);
        update
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(application_context());

        let any_base = update.into_any_base()?;
        ctx.apub.store_object(&any_base)?;

        Ok(any_base)
    }
}
