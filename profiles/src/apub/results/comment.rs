use super::{
    AnnounceCommentCreated, AnnounceCommentDeleted, AnnounceCommentUpdated, CommentCreated,
    CommentDeleted, CommentUpdated, LocalCommentCreated, LocalCommentUpdated, RemoteCommentCreated,
    RemoteCommentDeleted, RemoteCommentUpdated,
};
use crate::{
    apub::{ExtendedNote, Sensitive},
    store::Comment,
    Context, Error, OnBehalfOf, Outbound, Required,
};
use activitystreams::{
    activity::{Announce, Create, Delete, Update},
    base::AnyBase,
    context,
    object::{ApObject, Note},
    prelude::*,
    public, security,
};
use std::collections::HashSet;
use url::Url;
use uuid::Uuid;

fn comment_context() -> AnyBase {
    AnyBase::from_arbitrary_json(serde_json::json!({
        "sensitive": "as:sensitive"
    }))
    .unwrap()
}

fn local_inboxes(
    submission_id: Uuid,
    comment_id: Option<Uuid>,
    ctx: &Context,
) -> Result<Vec<Url>, Error> {
    let mut urls = vec![];

    let submissioner_id = ctx
        .store
        .submissions
        .by_id(submission_id)?
        .req("submission by id")?
        .profile_id();
    let submissioner_inbox = ctx
        .apub
        .endpoints_for_profile(submissioner_id)?
        .req("endpoints for submissioner")?
        .inbox()
        .to_owned();

    urls.push(submissioner_inbox);

    if let Some(comment_id) = comment_id {
        let commenter_id = ctx
            .store
            .comments
            .by_id(comment_id)?
            .req("comment by id")?
            .profile_id();

        if commenter_id != submissioner_id {
            let commenter_inbox = ctx
                .apub
                .endpoints_for_profile(commenter_id)?
                .req("endpoints for commenter")?
                .inbox()
                .to_owned();

            urls.push(commenter_inbox);
        }
    }

    Ok(urls)
}

fn remote_inboxes(submissioner_id: Uuid, ctx: &Context) -> Result<Vec<Url>, Error> {
    let follower_inboxes = ctx
        .store
        .followers_for(submissioner_id)
        .filter_map(|follower_id| {
            if !ctx.is_local(follower_id).ok()? {
                Some(
                    ctx.apub
                        .endpoints_for_profile(follower_id)
                        .ok()??
                        .inbox()
                        .to_owned(),
                )
            } else {
                None
            }
        })
        .collect::<HashSet<_>>();

    Ok(follower_inboxes.into_iter().collect())
}

fn build_comment(
    comment: Comment,
    note_id: Url,
    commenter_id: Url,
    ctx: &Context,
) -> Result<AnyBase, Error> {
    let published = comment.published().req("comment must be published")?;
    let endpoints = ctx
        .apub
        .endpoints_for_profile(comment.profile_id())?
        .req("endpoints for commenter")?;

    let submission_sensitive = ctx
        .store
        .submissions
        .by_id(comment.submission_id())?
        .req("submission by id")?
        .is_sensitive();

    let mut note = ExtendedNote::new(
        ApObject::new(Note::new()),
        Sensitive {
            sensitive: submission_sensitive,
        },
    );
    note.set_id(note_id)
        .set_content(comment.body())
        .set_published(published.into())
        .set_attributed_to(commenter_id)
        .add_cc(public());
    if let Some(updated) = comment.updated() {
        note.set_updated(updated.into());
    }
    if let Some(followers) = endpoints.followers {
        note.add_to(followers);
    }
    let in_reply_to = if let Some(comment_id) = comment.comment_id() {
        ctx.apub
            .apub_for_comment(comment_id)?
            .req("apub for comment")?
    } else {
        ctx.apub
            .apub_for_submission(comment.submission_id())?
            .req("apub for submission")?
    };
    note.set_in_reply_to(in_reply_to);

    let any_base = note.into_any_base()?;
    ctx.apub.store_object(&any_base)?;

    Ok(any_base)
}

impl Outbound for CommentCreated {
    fn id(&self) -> Option<Uuid> {
        Some(self.comment_id)
    }

    fn behalf(&self, ctx: &Context) -> Result<OnBehalfOf, Error> {
        let profile_id = ctx
            .store
            .comments
            .by_id(self.comment_id)?
            .req("comment by id")?
            .profile_id();

        Ok(OnBehalfOf::Profile(profile_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let comment = ctx
            .store
            .comments
            .by_id(self.comment_id)?
            .req("comment by id")?;

        local_inboxes(comment.submission_id(), comment.comment_id(), ctx)
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let comment = ctx
            .store
            .comments
            .by_id(self.comment_id)?
            .req("comment by id")?;
        let commenter_id = ctx
            .apub
            .apub_for_profile(comment.profile_id())?
            .req("apub for commenter")?;
        let note_id = ctx.apub.info.gen_id().req("id generation")?;
        ctx.apub.comment(&note_id, self.comment_id)?;
        let note = build_comment(comment, note_id, commenter_id.clone(), ctx)?;

        let mut create = Create::new(commenter_id, note);
        create
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(comment_context());
        let any_base = create.into_any_base()?;
        ctx.apub.store_object(&any_base)?;

        Ok(any_base)
    }
}

impl Outbound for LocalCommentCreated {
    fn id(&self) -> Option<Uuid> {
        Some(self.comment_id)
    }

    fn ready(&self) -> bool {
        false
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Err(Error::Invalid)
    }

    fn inboxes(&self, _: &Context) -> Result<Vec<Url>, Error> {
        Err(Error::Invalid)
    }

    fn to_apub(&self, _: &Context) -> Result<AnyBase, Error> {
        Err(Error::Invalid)
    }
}

impl Outbound for AnnounceCommentCreated {
    fn id(&self) -> Option<Uuid> {
        Some(self.comment_id)
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Profile(self.submissioner_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        remote_inboxes(self.submissioner_id, ctx)
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let comment = ctx
            .store
            .comments
            .by_id(self.comment_id)?
            .req("comment by id")?;
        let submissioner_id = ctx
            .apub
            .apub_for_profile(self.submissioner_id)?
            .req("apub for submissioner profile")?;
        let note_id = ctx.apub.info.gen_id().req("id generation")?;
        ctx.apub.comment(&note_id, self.comment_id)?;
        let note = build_comment(comment, note_id, submissioner_id.clone(), ctx)?;

        let mut announce = Announce::new(submissioner_id, note);
        announce
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(comment_context());
        let any_base = announce.into_any_base()?;
        ctx.apub.store_object(&any_base)?;

        Ok(any_base)
    }
}

impl Outbound for RemoteCommentCreated {
    fn id(&self) -> Option<Uuid> {
        None
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Profile(self.submissioner_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        remote_inboxes(self.submissioner_id, ctx)
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let comment = ctx
            .apub
            .object(&self.note_apub_id)?
            .req("apub for note id")?;
        let submissioner_id = ctx
            .apub
            .apub_for_profile(self.submissioner_id)?
            .req("apub for submissioner profile")?;

        let mut announce = Announce::new(submissioner_id, comment);
        announce
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(comment_context());
        let announce = announce.into_any_base()?;
        ctx.apub.store_object(&announce)?;

        Ok(announce)
    }
}

impl Outbound for CommentUpdated {
    fn id(&self) -> Option<Uuid> {
        Some(self.comment_id)
    }

    fn behalf(&self, ctx: &Context) -> Result<OnBehalfOf, Error> {
        let comment = ctx
            .store
            .comments
            .by_id(self.comment_id)?
            .req("comment by id")?;

        Ok(OnBehalfOf::Profile(comment.profile_id()))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let comment = ctx
            .store
            .comments
            .by_id(self.comment_id)?
            .req("comment by id")?;

        local_inboxes(comment.submission_id(), comment.comment_id(), ctx)
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let comment = ctx
            .store
            .comments
            .by_id(self.comment_id)?
            .req("comment by id")?;
        let commenter_id = ctx
            .apub
            .apub_for_profile(comment.profile_id())?
            .req("apub for commenter profile")?;
        let note_id = ctx
            .apub
            .apub_for_comment(comment.id())?
            .req("apub for comment")?;
        let note = build_comment(comment, note_id, commenter_id.clone(), ctx)?;

        let mut update = Update::new(commenter_id, note);
        update
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(comment_context());
        let any_base = update.into_any_base()?;
        ctx.apub.store_object(&any_base)?;

        Ok(any_base)
    }
}

impl Outbound for LocalCommentUpdated {
    fn id(&self) -> Option<Uuid> {
        Some(self.comment_id)
    }

    fn ready(&self) -> bool {
        false
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Err(Error::Invalid)
    }

    fn inboxes(&self, _: &Context) -> Result<Vec<Url>, Error> {
        Err(Error::Invalid)
    }

    fn to_apub(&self, _: &Context) -> Result<AnyBase, Error> {
        Err(Error::Invalid)
    }
}

impl Outbound for AnnounceCommentUpdated {
    fn id(&self) -> Option<Uuid> {
        Some(self.comment_id)
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Profile(self.submissioner_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        remote_inboxes(self.submissioner_id, ctx)
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let comment = ctx
            .store
            .comments
            .by_id(self.comment_id)?
            .req("comment by id")?;
        let commenter_id = ctx
            .apub
            .apub_for_profile(comment.profile_id())?
            .req("apub for commenter profile")?;
        let submissioner_id = ctx
            .apub
            .apub_for_profile(self.submissioner_id)?
            .req("apub for submissioner profile")?;
        let note_id = ctx
            .apub
            .apub_for_comment(comment.id())?
            .req("apub for comment")?;
        let note = build_comment(comment, note_id, commenter_id.clone(), ctx)?;

        let mut update = Update::new(commenter_id, note);
        update
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(comment_context());
        let update = update.into_any_base()?;
        ctx.apub.store_object(&update)?;

        let mut announce = Announce::new(submissioner_id, update);
        announce
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(comment_context());
        let announce = announce.into_any_base()?;
        ctx.apub.store_object(&announce)?;

        Ok(announce)
    }
}

impl Outbound for RemoteCommentUpdated {
    fn id(&self) -> Option<Uuid> {
        None
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Profile(self.submissioner_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        remote_inboxes(self.submissioner_id, ctx)
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let submissioner_id = ctx
            .apub
            .apub_for_profile(self.submissioner_id)?
            .req("apub for submissioner profile")?;
        let update = ctx
            .apub
            .object(&self.update_apub_id)?
            .req("apub for update")?;

        let mut announce = Announce::new(submissioner_id, update);
        announce
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(comment_context());
        let announce = announce.into_any_base()?;
        ctx.apub.store_object(&announce)?;

        Ok(announce)
    }
}

impl Outbound for CommentDeleted {
    fn id(&self) -> Option<Uuid> {
        None
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Profile(self.commenter_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        local_inboxes(self.submission_id, self.reply_to_id, ctx)
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let commenter_id = ctx
            .apub
            .apub_for_profile(self.commenter_id)?
            .req("apub for commenter profile")?;
        let note = ctx.apub.object(&self.note_apub_id)?.req("apub for note")?;

        let mut delete = Delete::new(commenter_id, note);
        delete
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(comment_context());
        let any_base = delete.into_any_base()?;
        ctx.apub.store_object(&any_base)?;

        Ok(any_base)
    }
}

impl Outbound for AnnounceCommentDeleted {
    fn id(&self) -> Option<Uuid> {
        None
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Profile(self.submissioner_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        remote_inboxes(self.submissioner_id, ctx)
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let commenter_id = ctx
            .apub
            .apub_for_profile(self.commenter_id)?
            .req("apub for commenter profile")?;
        let submissioner_id = ctx
            .apub
            .apub_for_profile(self.submissioner_id)?
            .req("apub for submissioner profile")?;
        let note = ctx.apub.object(&self.note_apub_id)?.req("apub for note")?;

        let mut delete = Delete::new(commenter_id, note);
        delete
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(comment_context());
        let delete = delete.into_any_base()?;
        ctx.apub.store_object(&delete)?;

        let mut announce = Announce::new(submissioner_id, delete);
        announce
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(comment_context());
        let announce = announce.into_any_base()?;
        ctx.apub.store_object(&announce)?;

        Ok(announce)
    }
}

impl Outbound for RemoteCommentDeleted {
    fn id(&self) -> Option<Uuid> {
        None
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Profile(self.submissioner_id))
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        remote_inboxes(self.submissioner_id, ctx)
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let submissioner_id = ctx
            .apub
            .apub_for_profile(self.submissioner_id)?
            .req("apub for submissioner profile")?;
        let delete = ctx
            .apub
            .object(&self.delete_apub_id)?
            .req("apub for delete")?;

        let mut announce = Announce::new(submissioner_id, delete);
        announce
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security())
            .add_context(comment_context());
        let announce = announce.into_any_base()?;
        ctx.apub.store_object(&announce)?;

        Ok(announce)
    }
}
