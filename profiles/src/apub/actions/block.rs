use crate::{
    apub::actions::{CreateBlock, DeleteBlock, RejectFollowRequest, UndoFollow},
    Action, Context, Error, Outbound,
};

impl Action for CreateBlock {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let block = ctx.store.view.blocks.new(
            self.blocked_profile,
            self.blocked_by_profile,
            self.published,
        )?;

        if let Some(apub_id) = &self.block_apub_id {
            ctx.apub.block(&apub_id, block.id)?;
        }

        if let Ok(Some(follow_request_id)) = ctx
            .store
            .view
            .follow_requests
            .by_forward(self.blocked_profile, self.blocked_by_profile)
        {
            Action::perform(&RejectFollowRequest { follow_request_id }, ctx)?;
        }

        if let Ok(Some(follow_request_id)) = ctx
            .store
            .view
            .follow_requests
            .by_forward(self.blocked_by_profile, self.blocked_profile)
        {
            Action::perform(&RejectFollowRequest { follow_request_id }, ctx)?;
        }

        if let Ok(Some(follow_id)) = ctx
            .store
            .view
            .follows
            .by_forward(self.blocked_profile, self.blocked_by_profile)
        {
            Action::perform(&UndoFollow { follow_id }, ctx)?;
        }

        if let Ok(Some(follow_id)) = ctx
            .store
            .view
            .follows
            .by_forward(self.blocked_by_profile, self.blocked_profile)
        {
            Action::perform(&UndoFollow { follow_id }, ctx)?;
        }

        if ctx.is_local(self.blocked_by_profile)? && !ctx.is_local(self.blocked_profile)? {
            return Ok(Some(Box::new(crate::apub::results::Block {
                block_id: block.id,
            })));
        }

        Ok(None)
    }
}

impl Action for DeleteBlock {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let opt = ctx.store.view.blocks.remove(self.block_id)?;
        if let Some(block_apub_id) = ctx.apub.apub_for_block(self.block_id)? {
            ctx.apub.delete_object(&block_apub_id)?;

            if let Some(undo_block) = opt {
                if ctx.is_local(undo_block.0.right)? && !ctx.is_local(undo_block.0.left)? {
                    return Ok(Some(Box::new(crate::apub::results::UndoBlock {
                        block_apub_id,
                        profile_id: undo_block.0.right,
                        blocked_id: undo_block.0.left,
                    })));
                }
            }
        }

        Ok(None)
    }
}
