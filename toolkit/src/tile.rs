use crate::image::Image;
use std::rc::Rc;

#[derive(Default)]
pub struct Tiles {
    small: bool,
    scroll: bool,
}

impl Tiles {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn small(mut self, small: bool) -> Self {
        self.small = small;
        self
    }

    pub fn scroll(mut self, scroll: bool) -> Self {
        self.scroll = scroll;
        self
    }

    pub fn class_string(&self) -> String {
        let mut classes = vec!["toolkit-tiles".to_owned()];

        if self.small {
            classes.push("toolkit-tiles__small".to_owned());
        }

        if self.scroll {
            classes.push("toolkit-tiles__scroll".to_owned());
        }

        classes.join(" ")
    }
}

#[derive(Clone, Copy, Debug)]
pub enum TileFlag {
    Normal,
    Primary,
}

#[derive(Clone, Copy, Debug)]
pub enum IndicatorColor {
    Red,
    White,
    Gray,
}

#[derive(Clone, Debug)]
pub(crate) struct Indicator {
    pub(crate) text: String,
    pub(crate) color: IndicatorColor,
}

impl Indicator {
    pub(crate) fn class_string(&self) -> String {
        let mut classes = vec!["toolkit-indicator".to_owned()];

        match self.color {
            IndicatorColor::Red => classes.push("toolkit-indicator__red".to_owned()),
            IndicatorColor::White => classes.push("toolkit-indicator__white".to_owned()),
            IndicatorColor::Gray => classes.push("toolkit-indicator__gray".to_owned()),
        }

        classes.join(" ")
    }
}

#[derive(Clone)]
pub struct Tile {
    pub(crate) title: Option<String>,
    pub(crate) description: Option<String>,
    pub(crate) link: Option<String>,
    pub(crate) indicator: Option<Indicator>,
    pub(crate) flag: TileFlag,
    image: Rc<dyn Image>,
}

impl Tile {
    pub fn new(image: impl Image + 'static) -> Self {
        Tile {
            title: None,
            description: None,
            link: None,
            indicator: None,
            flag: TileFlag::Normal,
            image: Rc::new(image),
        }
    }

    pub fn indicator(mut self, text: &str, color: IndicatorColor) -> Self {
        self.indicator = Some(Indicator {
            text: text.to_owned(),
            color,
        });
        self
    }

    pub fn title(mut self, title: &str) -> Self {
        self.title = Some(title.to_owned());
        self
    }

    pub fn description(mut self, description: &str) -> Self {
        self.description = Some(description.to_owned());
        self
    }

    pub fn link(mut self, link: &str) -> Self {
        self.link = Some(link.to_owned());
        self
    }

    pub fn flag(mut self, flag: TileFlag) -> Self {
        self.flag = flag;
        self
    }

    pub(crate) fn image(&self) -> &dyn Image {
        &*self.image
    }

    pub(crate) fn class_string(&self) -> String {
        let mut classes = vec!["toolkit-tile".to_owned()];

        match self.flag {
            TileFlag::Normal => (),
            TileFlag::Primary => {
                classes.push("toolkit-tile__primary".to_owned());
            }
        }

        classes.join(" ")
    }
}

impl std::fmt::Debug for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("Tile")
            .field("title", &self.title)
            .field("description", &self.description)
            .field("link", &self.link)
            .field("indicator", &self.indicator)
            .field("flag", &self.flag)
            .field("image", &"Box<dyn Image>")
            .finish()
    }
}
