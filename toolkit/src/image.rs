use std::rc::Rc;

pub trait Image {
    fn src(&self) -> String;

    fn title(&self) -> String;

    fn png_srcset(&self) -> Option<String>;
    fn jpeg_srcset(&self) -> Option<String>;
    fn webp_srcset(&self) -> Option<String>;
}

impl<T> Image for &T
where
    T: Image,
{
    fn src(&self) -> String {
        T::src(self)
    }

    fn title(&self) -> String {
        T::title(self)
    }

    fn png_srcset(&self) -> Option<String> {
        T::png_srcset(self)
    }

    fn jpeg_srcset(&self) -> Option<String> {
        T::jpeg_srcset(self)
    }

    fn webp_srcset(&self) -> Option<String> {
        T::webp_srcset(self)
    }
}

impl<T> Image for Rc<T>
where
    T: Image,
{
    fn src(&self) -> String {
        T::src(self)
    }

    fn title(&self) -> String {
        T::title(self)
    }

    fn png_srcset(&self) -> Option<String> {
        T::png_srcset(self)
    }

    fn jpeg_srcset(&self) -> Option<String> {
        T::jpeg_srcset(self)
    }

    fn webp_srcset(&self) -> Option<String> {
        T::webp_srcset(self)
    }
}

impl<T> Image for Box<T>
where
    T: Image,
{
    fn src(&self) -> String {
        T::src(self)
    }

    fn title(&self) -> String {
        T::title(self)
    }

    fn png_srcset(&self) -> Option<String> {
        T::png_srcset(self)
    }

    fn jpeg_srcset(&self) -> Option<String> {
        T::jpeg_srcset(self)
    }

    fn webp_srcset(&self) -> Option<String> {
        T::webp_srcset(self)
    }
}
