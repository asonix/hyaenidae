use actix_web::{http::StatusCode, HttpResponse, ResponseError};

pub(crate) trait OptionExt<T> {
    fn req(self) -> Result<T, Error>;
}

#[derive(Debug, thiserror::Error)]
pub(crate) enum Error {
    #[error("{0}")]
    Accounts(#[from] hyaenidae_accounts::Error),

    #[error("{0}")]
    Profiles(#[from] hyaenidae_profiles::Error),

    #[error("{0}")]
    Render(std::io::Error),

    #[error("{0}")]
    Sled(#[from] sled::Error),

    #[error("{0}")]
    Transaction(#[from] sled::transaction::TransactionError),

    #[error("{0}")]
    Json(#[from] serde_json::Error),

    #[error("{0}")]
    Url(#[from] url::ParseError),

    #[error("{0}")]
    Webfinger(#[from] actix_webfinger::FetchError),

    #[error("Required data was not present")]
    Required,

    #[error("Panic in blocking operation")]
    Panic,
}

impl ResponseError for Error {
    fn status_code(&self) -> StatusCode {
        StatusCode::SEE_OTHER
    }

    fn error_response(&self) -> HttpResponse {
        if matches!(self, Error::Required) {
            return crate::to_404();
        }

        crate::to_500()
    }
}

impl<T> OptionExt<T> for Option<T> {
    fn req(self) -> Result<T, Error> {
        self.ok_or_else(|| Error::Required)
    }
}

impl From<hyaenidae_profiles::store::StoreError> for Error {
    fn from(e: hyaenidae_profiles::store::StoreError) -> Self {
        Error::Profiles(From::from(e))
    }
}

impl From<hyaenidae_profiles::apub::StoreError> for Error {
    fn from(e: hyaenidae_profiles::apub::StoreError) -> Self {
        Error::Profiles(From::from(e))
    }
}

impl From<actix_web::error::BlockingError> for Error {
    fn from(_: actix_web::error::BlockingError) -> Self {
        Self::Panic
    }
}
