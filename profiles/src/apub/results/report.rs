use super::ReportCreated;
use crate::{
    store::{OwnerSource, Report, ReportKind},
    Context, Error, OnBehalfOf, Outbound, Required,
};
use activitystreams::{activity::Flag, base::AnyBase, context, prelude::*, security};
use url::Url;
use uuid::Uuid;

fn server_from_report(report: &Report, ctx: &Context) -> Result<Uuid, Error> {
    let profile = match report.kind() {
        ReportKind::Profile => ctx
            .store
            .profiles
            .by_id(report.item())?
            .req("profile by id")?,
        ReportKind::Submission => {
            let submission = ctx
                .store
                .submissions
                .by_id(report.item())?
                .req("submission by id")?;
            ctx.store
                .profiles
                .by_id(submission.profile_id())?
                .req("profile by id")?
        }
        ReportKind::Comment => {
            let comment = ctx
                .store
                .comments
                .by_id(report.item())?
                .req("comment by id")?;
            ctx.store
                .profiles
                .by_id(comment.profile_id())?
                .req("profile by id")?
        }
        ReportKind::Post => {
            unimplemented!()
        }
    };

    if let OwnerSource::Remote(server_id) = profile.owner_source() {
        Ok(*server_id)
    } else {
        Err(Error::Invalid)
    }
}

impl Outbound for ReportCreated {
    fn id(&self) -> Option<Uuid> {
        Some(self.report_id)
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Server)
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let report = ctx
            .store
            .reports
            .by_id(self.report_id)?
            .req("report by id")?;

        let server_id = server_from_report(&report, ctx)?;

        let inbox = ctx
            .apub
            .endpoints_for_server(server_id)?
            .req("endpoints for server")?
            .inbox()
            .to_owned();

        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let report = ctx
            .store
            .reports
            .by_id(self.report_id)?
            .req("report by id")?;
        let server_id = server_from_report(&report, ctx)?;
        let actor_id = ctx
            .apub
            .apub_for_server(server_id)?
            .req("apub id for server")?;

        let endpoints = ctx
            .apub
            .endpoints_for_server(server_id)?
            .req("endpoints for server")?;

        let apub_id = match report.kind() {
            ReportKind::Profile => ctx
                .apub
                .apub_for_profile(report.item())?
                .req("apub id for profile")?,
            ReportKind::Submission => ctx
                .apub
                .apub_for_submission(report.item())?
                .req("apub id for submission")?,
            ReportKind::Comment => ctx
                .apub
                .apub_for_comment(report.item())?
                .req("apub id for comment")?,
            ReportKind::Post => {
                unimplemented!()
            }
        };

        let mut flag = Flag::new(actor_id.clone(), apub_id);
        flag.set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security());
        if let Some(followers) = endpoints.followers {
            flag.add_to(followers);
        }
        if let Some(note) = report.note() {
            flag.set_content(note);
        }

        let flag = flag.into_any_base()?;
        ctx.apub.store_object(&flag)?;

        Ok(flag)
    }
}
