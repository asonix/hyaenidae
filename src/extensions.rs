use hyaenidae_profiles::store::{Comment, Profile, Submission};

pub(crate) trait ProfileExt {
    fn name(&self) -> String;
    fn description_text(&self) -> Option<&str>;
    fn full_handle(&self) -> String;
    fn view_path(&self) -> String;
    fn follow_path(&self) -> String;
    fn unfollow_path(&self) -> String;
    fn block_path(&self) -> String;
    fn unblock_path(&self) -> String;
    fn report_path(&self) -> String;
}

pub(crate) trait SubmissionExt {
    fn view_path(&self) -> String;
    fn report_path(&self) -> String;
    fn update_path(&self) -> String;
    fn comment_path(&self) -> String;
    fn title_text(&self) -> String;
    fn has_title(&self) -> bool;
    fn description_text(&self) -> Option<&str>;
    fn file_page_link(&self, page: usize) -> String;
}

pub(crate) trait CommentExt {
    fn view_path(&self) -> String;
    fn edit_path(&self) -> String;
    fn reply_path(&self) -> String;
    fn report_path(&self) -> String;
}

impl CommentExt for Comment {
    fn view_path(&self) -> String {
        format!("/comments/{}", self.id())
    }

    fn edit_path(&self) -> String {
        format!("{}/edit", self.view_path())
    }

    fn reply_path(&self) -> String {
        format!("{}/reply", self.view_path())
    }

    fn report_path(&self) -> String {
        format!("{}/report", self.view_path())
    }
}

impl SubmissionExt for Submission {
    fn view_path(&self) -> String {
        format!("/submissions/{}", self.id())
    }

    fn report_path(&self) -> String {
        format!("{}/report", self.view_path())
    }

    fn update_path(&self) -> String {
        format!("{}/update", self.view_path())
    }

    fn comment_path(&self) -> String {
        format!("{}/comment", self.view_path())
    }

    fn title_text(&self) -> String {
        if self.published().is_none() {
            if !self.has_title() {
                "Not Published".to_owned()
            } else {
                format!("Not Published: {}", self.title())
            }
        } else {
            self.title().to_owned()
        }
    }

    fn has_title(&self) -> bool {
        !self.title().trim().is_empty()
    }

    fn description_text(&self) -> Option<&str> {
        if let Some(description) = self.description() {
            if description.trim().len() == 0 {
                None
            } else {
                Some(description)
            }
        } else {
            None
        }
    }

    fn file_page_link(&self, page: usize) -> String {
        format!("{}?file_page={}", self.view_path(), page)
    }
}

impl ProfileExt for Profile {
    fn name(&self) -> String {
        self.display_name()
            .map(|dn| dn.to_owned())
            .unwrap_or(self.full_handle())
    }

    fn description_text(&self) -> Option<&str> {
        if self.is_suspended() {
            Some("profile suspended")
        } else {
            self.description()
        }
    }

    fn full_handle(&self) -> String {
        format!("@{}@{}", self.handle(), self.domain())
    }

    fn view_path(&self) -> String {
        format!("/profiles/{}", self.full_handle())
    }

    fn follow_path(&self) -> String {
        format!("{}/follow", self.view_path())
    }

    fn unfollow_path(&self) -> String {
        format!("{}/unfollow", self.view_path())
    }

    fn block_path(&self) -> String {
        format!("{}/block", self.view_path())
    }

    fn unblock_path(&self) -> String {
        format!("{}/unblock", self.view_path())
    }

    fn report_path(&self) -> String {
        format!("{}/report", self.view_path())
    }
}
