#[derive(Clone, Copy, Debug)]
pub enum ButtonKind {
    Primary,
    PrimaryOutline,
    PrimaryLink,
    Secondary,
    Outline,
    Link,
}

#[derive(Clone, Copy, Debug)]
pub enum LinkKind {
    CurrentTab,
    NewTab,
}

#[derive(Clone, Debug, Default)]
pub struct Button {
    pub(crate) label: String,
    pub(crate) kind: ButtonKind,
    pub(crate) classes: Vec<String>,
    pub(crate) href: Option<String>,
    pub(crate) link_kind: LinkKind,
    pub(crate) action: Option<String>,
}

impl Button {
    pub fn primary(label: &str) -> Self {
        Button {
            label: label.to_owned(),
            ..Default::default()
        }
    }

    pub fn primary_outline(label: &str) -> Self {
        Button {
            label: label.to_owned(),
            kind: ButtonKind::PrimaryOutline,
            ..Default::default()
        }
    }

    pub fn primary_link(label: &str) -> Self {
        Button {
            label: label.to_owned(),
            kind: ButtonKind::PrimaryLink,
            ..Default::default()
        }
    }

    pub fn outline(label: &str) -> Self {
        Button {
            label: label.to_owned(),
            kind: ButtonKind::Outline,
            ..Default::default()
        }
    }

    pub fn link(label: &str) -> Self {
        Button {
            label: label.to_owned(),
            kind: ButtonKind::Link,
            ..Default::default()
        }
    }

    pub fn secondary(label: &str) -> Self {
        Button {
            label: label.to_owned(),
            kind: ButtonKind::Secondary,
            ..Default::default()
        }
    }

    pub fn kind(mut self, kind: ButtonKind) -> Self {
        self.kind = kind;
        self
    }

    pub fn class(mut self, class: &str) -> Self {
        self.classes.push(class.to_owned());
        self
    }

    pub fn href(mut self, href: &str) -> Self {
        self.href = Some(href.to_owned());
        self
    }

    pub fn new_tab(mut self) -> Self {
        self.link_kind = LinkKind::NewTab;
        self
    }

    pub fn form(mut self, action: &str) -> Self {
        self.action = Some(action.to_owned());
        self
    }

    pub(crate) fn class_string(&self) -> String {
        use ButtonKind::*;

        let mut classes = self.classes.clone();

        classes.push("toolkit-button".to_owned());

        let static_classes = match self.kind {
            Primary => "toolkit-button__primary",
            PrimaryOutline => "toolkit-button__primary-outline",
            PrimaryLink => "toolkit-button__primary-link",
            Secondary => "toolkit-button__secondary",
            Outline => "toolkit-button__outline",
            Link => "toolkit-button__link",
        };

        classes.push(static_classes.to_owned());

        classes.join(" ")
    }
}

impl Default for ButtonKind {
    fn default() -> Self {
        ButtonKind::Primary
    }
}

impl Default for LinkKind {
    fn default() -> Self {
        LinkKind::CurrentTab
    }
}
