use crate::image::Image;
use std::rc::Rc;

#[derive(Clone, Copy, Debug)]
pub enum Size {
    Normal,
    Small,
    Tiny,
}

#[derive(Clone)]
pub struct Icon {
    pub(crate) href: String,
    pub(crate) dark: bool,
    classes: Vec<String>,
    size: Size,
    image: Option<Rc<dyn Image>>,
}

impl Icon {
    pub fn new(href: String) -> Self {
        Icon {
            href,
            size: Size::Normal,
            classes: vec![],
            dark: false,
            image: None,
        }
    }

    pub fn image(mut self, image: impl Image + 'static) -> Self {
        self.image = Some(Rc::new(image));
        self
    }

    pub fn dark(mut self, dark: bool) -> Self {
        self.dark = dark;
        self
    }

    pub fn size(mut self, size: Size) -> Self {
        self.size = size;
        self
    }

    pub fn class(mut self, class: &str) -> Self {
        self.classes.push(class.to_owned());
        self
    }

    pub(crate) fn image_opt(&self) -> Option<&dyn Image> {
        self.image.as_deref()
    }

    pub(crate) fn class_string(&self) -> String {
        let mut classes = self.classes.clone();

        classes.push("toolkit-icon--link".to_owned());

        match self.size {
            Size::Normal => (),
            Size::Small => classes.push("toolkit-icon--link__small".to_owned()),
            Size::Tiny => classes.push("toolkit-icon--link__tiny".to_owned()),
        }

        if self.dark {
            classes.push("toolkit-dark".to_owned());
        }

        classes.join(" ")
    }
}

impl std::fmt::Debug for Icon {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("Icon")
            .field("href", &self.href)
            .field("small", &self.dark)
            .field("dark", &self.dark)
            .field("image", &"Rc<dyn Image>")
            .finish()
    }
}
