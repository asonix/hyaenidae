#[derive(Clone, Debug, Default)]
pub struct SelectOption {
    pub value: String,
    pub text: String,
}

#[derive(Clone, Debug, Default)]
pub struct Select {
    pub(crate) name: String,
    pub(crate) options: Vec<SelectOption>,
    pub(crate) default: Option<String>,
    pub(crate) title: Option<String>,
    dark: bool,
}

impl Select {
    pub fn new(name: &str) -> Self {
        Select {
            name: name.to_owned(),
            title: None,
            options: vec![],
            default: None,
            dark: false,
        }
    }

    pub fn title(mut self, title: &str) -> Self {
        self.title = Some(title.to_owned());
        self
    }

    pub fn default_option(mut self, value: &str) -> Self {
        self.default = Some(value.to_owned());
        self
    }

    pub fn add_option(mut self, text: String, value: String) -> Self {
        self.options.push(SelectOption { value, text });
        self
    }

    pub fn options(mut self, options: &[(&str, &str)]) -> Self {
        self.options = options
            .iter()
            .map(|(text, value)| SelectOption {
                text: text.to_string(),
                value: value.to_string(),
            })
            .collect();
        self
    }

    pub fn dark(mut self, dark: bool) -> Self {
        self.dark = dark;
        self
    }

    pub(crate) fn class_string(&self) -> String {
        let mut classes = vec![];

        classes.push("toolkit-select".to_owned());

        if self.dark {
            classes.push("toolkit-dark".to_owned());
        }

        classes.join(" ")
    }
}
