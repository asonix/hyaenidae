use ructe::Ructe;

fn main() -> ructe::Result<()> {
    let mut ructe = Ructe::from_env()?;
    ructe.compile_templates("templates")?;

    Ok(())
}
