use crate::image::Image;
use std::rc::Rc;

#[derive(Clone, Default)]
pub struct Banner {
    image: Option<Rc<dyn Image>>,
    dark: bool,
}

impl Banner {
    pub fn new() -> Self {
        Banner {
            image: None,
            dark: false,
        }
    }

    pub fn image(mut self, image: impl Image + 'static) -> Self {
        self.image = Some(Rc::new(image));
        self
    }

    pub fn dark(mut self, dark: bool) -> Self {
        self.dark = dark;
        self
    }

    pub(crate) fn image_opt(&self) -> Option<&dyn Image> {
        self.image.as_deref()
    }

    pub(crate) fn class_string(&self) -> String {
        let mut classes = vec!["toolkit-banner".to_owned()];

        if self.dark {
            classes.push("toolkit-dark".to_owned());
        }

        classes.join(" ")
    }
}

impl std::fmt::Debug for Banner {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("Banner")
            .field("dark", &self.dark)
            .field("image", &"Rc<dyn Image>")
            .finish()
    }
}
