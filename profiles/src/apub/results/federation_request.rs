use super::{
    AcceptFederation, Federation as FederationRequested, RejectFederation, UndoAcceptFederation,
    UndoFederation,
};
use crate::{Context, Error, OnBehalfOf, Outbound, Required};
use activitystreams::{
    activity::{Accept, Follow, Reject, Undo},
    base::AnyBase,
    context,
    prelude::*,
    security,
};
use url::Url;
use uuid::Uuid;

impl Outbound for FederationRequested {
    fn id(&self) -> Option<Uuid> {
        Some(self.follow_request_id)
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Server)
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let follow = ctx
            .store
            .view
            .server_follow_requests
            .by_id(self.follow_request_id)?
            .req("server follow request by id")?;
        let inbox = ctx
            .apub
            .endpoints_for_server(follow.left)?
            .req("endpoints for followed server")?
            .inbox()
            .to_owned();

        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let follow = ctx
            .store
            .view
            .server_follow_requests
            .by_id(self.follow_request_id)?
            .req("server follow request by id")?;
        let actor_id = ctx
            .apub
            .apub_for_server(follow.right)?
            .req("apub for follower server")?;
        let object_id = ctx
            .apub
            .apub_for_server(follow.left)?
            .req("apub for followed server")?;
        let published = follow.published;

        let follow_id = ctx.apub.info.gen_id().req("id generation")?;
        ctx.apub
            .server_follow_request(&follow_id, self.follow_request_id)?;

        let mut follow = Follow::new(actor_id, object_id);
        follow
            .set_id(follow_id)
            .set_published(published.into())
            .add_context(context())
            .add_context(security());
        let follow = follow.into_any_base()?;
        ctx.apub.store_object(&follow)?;

        Ok(follow)
    }
}

impl Outbound for RejectFederation {
    fn id(&self) -> Option<Uuid> {
        None
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Server)
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let inbox = ctx
            .apub
            .endpoints_for_server(self.requester_id)?
            .req("endpoints for requesting server")?
            .inbox()
            .to_owned();

        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let application_id = ctx
            .apub
            .apub_for_server(self.server_id)?
            .req("apub for server")?;
        let follow_request = ctx
            .apub
            .object(&self.follow_apub_id)?
            .req("apub for follow")?;

        let mut reject = Reject::new(application_id, follow_request);
        reject
            .set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security());
        let reject = reject.into_any_base()?;
        ctx.apub.store_object(&reject)?;

        Ok(reject)
    }
}

impl Outbound for AcceptFederation {
    fn id(&self) -> Option<Uuid> {
        Some(self.follow_id)
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Server)
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let requester_id = ctx
            .store
            .view
            .server_follows
            .right(self.follow_id)?
            .req("requesting server for follow")?;

        let inbox = ctx
            .apub
            .endpoints_for_server(requester_id)?
            .req("endpoints for requesting server")?
            .inbox()
            .to_owned();

        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let application_id = ctx
            .apub
            .apub_for_server(self.server_id)?
            .req("apub id for server id")?;
        let follow_request = ctx
            .apub
            .object(&self.follow_apub_id)?
            .req("apub for follow")?;

        let accept_id = ctx.apub.info.gen_id().req("id generation")?;
        ctx.apub.server_follow(&accept_id, self.follow_id)?;

        let mut accept = Accept::new(application_id, follow_request);
        accept
            .set_id(accept_id)
            .add_context(context())
            .add_context(security());
        let accept = accept.into_any_base()?;
        ctx.apub.store_object(&accept)?;

        Ok(accept)
    }
}

impl Outbound for UndoAcceptFederation {
    fn id(&self) -> Option<Uuid> {
        None
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Server)
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let inbox = ctx
            .apub
            .endpoints_for_server(self.requester_id)?
            .req("endpoints for requesting server")?
            .inbox()
            .to_owned();

        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let application_id = ctx
            .apub
            .apub_for_server(self.server_id)?
            .req("apub id for server")?;
        let accept = ctx
            .apub
            .object(&self.accept_apub_id)?
            .req("apub for accept")?;

        let mut undo = Undo::new(application_id, accept);
        undo.set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security());
        let undo = undo.into_any_base()?;
        ctx.apub.store_object(&undo)?;

        Ok(undo)
    }
}

impl Outbound for UndoFederation {
    fn id(&self) -> Option<Uuid> {
        None
    }

    fn behalf(&self, _: &Context) -> Result<OnBehalfOf, Error> {
        Ok(OnBehalfOf::Server)
    }

    fn inboxes(&self, ctx: &Context) -> Result<Vec<Url>, Error> {
        let inbox = ctx
            .apub
            .endpoints_for_server(self.followed_id)?
            .req("endpoints for followed server")?
            .inbox()
            .to_owned();

        Ok(vec![inbox])
    }

    fn to_apub(&self, ctx: &Context) -> Result<AnyBase, Error> {
        let application_id = ctx
            .apub
            .apub_for_server(self.server_id)?
            .req("apub id for server")?;
        let follow = ctx
            .apub
            .object(&self.follow_apub_id)?
            .req("apub for follow")?;

        let mut undo = Undo::new(application_id, follow);
        undo.set_id(ctx.apub.info.gen_id().req("id generation")?)
            .add_context(context())
            .add_context(security());
        let undo = undo.into_any_base()?;
        ctx.apub.store_object(&undo)?;

        Ok(undo)
    }
}
