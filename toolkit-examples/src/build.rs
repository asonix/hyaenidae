use ructe::Ructe;

fn main() -> ructe::Result<()> {
    let mut ructe = Ructe::from_env()?;
    let mut statics = ructe.statics()?;
    statics.add_sass_file("scss/layout.scss")?;
    ructe.compile_templates("templates")?;

    Ok(())
}
