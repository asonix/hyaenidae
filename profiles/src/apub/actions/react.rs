use crate::{
    apub::actions::{CreateReact, DeleteReact},
    Action, Context, Error, Outbound, Required,
};

impl Action for CreateReact {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let submissioner_id = ctx
            .store
            .submissions
            .by_id(self.submission_id)?
            .req("submission by id")?
            .profile_id();

        ctx.check_block(submissioner_id, self.profile_id)?;

        let commenter_id = if let Some(comment_id) = self.comment_id {
            let commenter_id = ctx
                .store
                .comments
                .by_id(comment_id)?
                .req("comment by id")?
                .profile_id();

            ctx.check_block(commenter_id, self.profile_id)?;

            Some(commenter_id)
        } else {
            None
        };

        let react = ctx.store.reacts.create(
            self.submission_id,
            self.profile_id,
            self.comment_id,
            &self.react,
            self.published,
        )?;

        if let Some(apub_id) = &self.like_apub_id {
            ctx.apub.react(&apub_id, react.id())?;
        }

        let notifier_id = commenter_id.unwrap_or(submissioner_id);

        if let Ok(Some(true)) = ctx.store.profiles.is_local(notifier_id) {
            ctx.store
                .view
                .reacts
                .new(notifier_id, react.id(), self.published);
        }

        if ctx.is_local(react.profile_id())? {
            return Ok(Some(Box::new(crate::apub::results::React {
                react_id: react.id(),
            })));
        }

        Ok(None)
    }
}

impl Action for DeleteReact {
    fn perform(&self, ctx: &Context) -> Result<Option<Box<dyn Outbound + Send>>, Error> {
        let react_id = self.react_id;
        let opt = ctx.store.reacts.delete(react_id)?;

        if let Some(undo_react) = opt {
            ctx.store.view.reacts.remove(react_id);

            let like_apub_id = ctx.apub.apub_for_react(react_id)?.req("apub for react")?;
            ctx.apub.delete_object(&like_apub_id)?;

            let profile_id = undo_react.0.profile_id();

            if ctx.is_local(profile_id)? {
                return Ok(Some(Box::new(crate::apub::results::UndoReact {
                    like_apub_id,
                    profile_id,
                    submission_id: undo_react.0.submission_id(),
                    comment_id: undo_react.0.comment_id(),
                })));
            }
        }

        Ok(None)
    }
}
