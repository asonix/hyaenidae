(function(fn) {
    if (document.readyState === "complete" || document.readyState === "interactive") {
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
})(function() {
    function onClick(nav) {
        return function _listener(event) {
            event.preventDefault();
            var containsOpen = false;

            for (var i = 0; i < nav.classList.length; i++) {
                if (nav.classList[i] == "nav-open") {
                    containsOpen = true;
                }
            }

            if (containsOpen) {
                nav.setAttribute("class", "nav-body nav-closing");
                setTimeout(function() {
                    nav.setAttribute("class", "nav-body nav-closed");
                }, 500)
            } else {
                nav.setAttribute("class", "nav-body nav-open");
            }
        };
    }

    var navs = document.getElementsByClassName("nav-body");
    var nav = navs[0];
    if (!nav) {
        return;
    }

    var links = document.getElementsByClassName("nav-link");

    for (var i = 0; i < links.length; i++) {
        var link = links[i];

        if (!link) {
            continue;
        }

        link.addEventListener("click", onClick(nav));
    }
})
